var express = require('express');
var router = express.Router();
var path = require('path');

// send all requests to index.html so browserHistory in React Router works
router.get('/', (req, res) => {
  res.render(path.resolve(path.join(__dirname, '../views/index.jade')));
});

router.get('/video', (req, res) => {
  res.sendFile(path.resolve(path.join(__dirname, '../views/video.html')));
});

module.exports = router;
