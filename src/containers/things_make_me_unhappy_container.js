import { connect } from 'react-redux';
import ThingsMakeMeUnhappy from 'srcRoot/components/things_make_me_unhappy';
import { actions } from 'srcRoot/redux/unhappy_items';
import { actions as timelineActions } from 'srcRoot/redux/timeline';

export function mapStateToProps({ unhappyItems }) {
  return {
    unhappyItems,
  };
}

export function mapDispatchToProps(dispatch) {
  return {
    updateUnhappyItems: (data) => dispatch(actions.updateUnhappyItems(data)),
    addEvent: (data) => dispatch(timelineActions.addEvent(data)),
  };
}

const ThingsMakeMeUnhappyContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ThingsMakeMeUnhappy);

export default ThingsMakeMeUnhappyContainer;
