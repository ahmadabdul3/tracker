import { connect } from 'react-redux';
import AppMenu from 'srcRoot/sections/app_menu';
import { actions } from 'srcRoot/redux/app_menu';

export function mapStateToProps({ appMenu }) {
  return {
    appMenu,
  };
}

export function mapDispatchToProps(dispatch) {
  return {
    hideAppMenu: () => dispatch(actions.hideAppMenu()),
  };
}

const AppMenuContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(AppMenu);

export default AppMenuContainer;
