import { connect } from 'react-redux';
import LoginPage from 'srcRoot/pages/login_page';
import { actions } from 'srcRoot/redux/login';

export function mapStateToProps({ }) {
  return {};
}

export function mapDispatchToProps(dispatch) {
  return {
    login: (data) => dispatch(actions.login(data)),
  };
}

const LoginPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);

export default LoginPageContainer;
