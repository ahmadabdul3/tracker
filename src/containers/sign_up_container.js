import { connect } from 'react-redux';
import SignUpPage from 'srcRoot/pages/sign_up';
import { actions as alertActions } from 'srcRoot/redux/alerts';

export function mapStateToProps({ }) {
  return {};
}

export function mapDispatchToProps(dispatch) {
  return {
    alertInfo: (data) => dispatch(alertActions.alertInfo(data)),
  };
}

const SignUpPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUpPage);

export default SignUpPageContainer;
