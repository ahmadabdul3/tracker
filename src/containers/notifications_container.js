import { connect } from 'react-redux';
import notificationsPage from 'srcRoot/pages/notifications';
import { actions } from 'srcRoot/redux/notifications';

export function mapStateToProps({ notifications }) {
  return {
    notifications,
  };
}

export function mapDispatchToProps(dispatch) {
  return {};
}

const NotificationsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(notificationsPage);

export default NotificationsContainer;
