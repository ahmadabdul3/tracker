import { connect } from 'react-redux';
import ActivityZonesForm from 'srcRoot/components/activity_zones_form';
import { actions } from 'srcRoot/redux/activity_zones';
import { actions as timelineActions } from 'srcRoot/redux/timeline';
import { actions as scheduledActivitiesActions } from 'srcRoot/redux/scheduled_activities';
import { actions as notificationActions } from 'srcRoot/redux/notifications';
import { actions as alertActions } from 'srcRoot/redux/alerts';

export function mapStateToProps({ happyItems, activityZones, scheduledActivities }) {
  return {
    happyItems,
    activityZones,
    scheduledActivities,
  };
}

export function mapDispatchToProps(dispatch) {
  return {
    addActivityZone: (data) => dispatch(actions.addActivityZone(data)),
    addEvent: (data) => dispatch(timelineActions.addEvent(data)),
    scheduleActivity: (data) => dispatch(scheduledActivitiesActions.scheduleActivity(data)),
    addNotification: (data) => dispatch(notificationActions.addNotification(data)),
    alertInfo: (data) => dispatch(alertActions.alertInfo(data)),
  };
}

const ActivityZonesFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ActivityZonesForm);

export default ActivityZonesFormContainer;
