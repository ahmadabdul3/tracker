import { connect } from 'react-redux';
import AppointmentsLink from 'srcRoot/components/appointments_link';

export function mapStateToProps({ appointments }) {
  return {
    appointments,
  };
}

export function mapDispatchToProps(dispatch) {
  return {};
}

const AppointmentsLinkContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(AppointmentsLink);

export default AppointmentsLinkContainer;
