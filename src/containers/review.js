import { connect } from 'react-redux';
import TherapistReview from 'srcRoot/pages/therapist_review';
import { actions } from 'srcRoot/redux/review';

export function mapStateToProps(state) {
  return {};
}

export function mapDispatchToProps(dispatch) {
  return {
    addReview: (data) => dispatch(actions.addReview(data)),
  };
}

const TherapistReviewContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TherapistReview);

export default TherapistReviewContainer;
