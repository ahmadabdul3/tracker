import { connect } from 'react-redux';
import ThingsMakeMeHappy from 'srcRoot/components/things_make_me_happy';
import { actions } from 'srcRoot/redux/happy_items';
import { actions as timelineActions } from 'srcRoot/redux/timeline';

export function mapStateToProps({ happyItems }) {
  return {
    happyItems,
  };
}

export function mapDispatchToProps(dispatch) {
  return {
    updateHappyItems: (data) => dispatch(actions.updateHappyItems(data)),
    addEvent: (data) => dispatch(timelineActions.addEvent(data)),
  };
}

const ThingsMakeMeHappyContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ThingsMakeMeHappy);

export default ThingsMakeMeHappyContainer;
