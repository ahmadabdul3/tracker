import { connect } from 'react-redux';
import ChatPage from 'srcRoot/pages/chat_page';

export function mapStateToProps({ login }) {
  return {
    login,
  };
}

export function mapDispatchToProps(dispatch) {
  return {};
}

const ChatPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatPage);

export default ChatPageContainer;
