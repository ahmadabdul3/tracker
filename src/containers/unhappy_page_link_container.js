import { connect } from 'react-redux';
import UnhappyPageLink from 'srcRoot/components/unhappy_page_link';

export function mapStateToProps({ unhappyItems }) {
  return {
    unhappyItems,
  };
}

export function mapDispatchToProps(dispatch) {
  return {};
}

const UnhappyPageLinkContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UnhappyPageLink);

export default UnhappyPageLinkContainer;
