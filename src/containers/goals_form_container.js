import { connect } from 'react-redux';
import GoalsForm from 'srcRoot/components/goals_form';
import { actions } from 'srcRoot/redux/goals_form';
import { actions as timelineActions } from 'srcRoot/redux/timeline';

export function mapStateToProps({ goals }) {
  return {
    goals,
  };
}

export function mapDispatchToProps(dispatch) {
  return {
    updateGoals: (data) => dispatch(actions.updateGoals(data)),
    addEvent: (data) => dispatch(timelineActions.addEvent(data)),
  };
}

const GoalsFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(GoalsForm);

export default GoalsFormContainer;
