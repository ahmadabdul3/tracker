import { connect } from 'react-redux';
import GoalsPageLink from 'srcRoot/components/goals_page_link';

export function mapStateToProps({ goals }) {
  return {
    goals,
  };
}

export function mapDispatchToProps(dispatch) {
  return {};
}

const GoalsPageLinkContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(GoalsPageLink);

export default GoalsPageLinkContainer;
