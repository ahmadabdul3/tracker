import { connect } from 'react-redux';
import HowAreYouFeelingToday from 'srcRoot/components/how_are_you_feeling_today';
import { actions } from 'srcRoot/redux/daily_status';
import { actions as timelineActions } from 'srcRoot/redux/timeline';

export function mapStateToProps({ dailyStatus }) {
  return {};
}

export function mapDispatchToProps(dispatch) {
  return {
    submitDailyStatus: (data) => dispatch(actions.submitDailyStatus(data)),
    addEvent: (data) => dispatch(timelineActions.addEvent(data)),
  };
}

const DailyStatusContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(HowAreYouFeelingToday);

export default DailyStatusContainer;
