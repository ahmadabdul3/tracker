import { connect } from 'react-redux';
import TherapyTimelinePage from 'srcRoot/pages/therapy_timeline';

export function mapStateToProps({ timeline, appointments }) {
  return {
    timeline,
    appointments,
  };
}

export function mapDispatchToProps(dispatch) {
  return {};
}

const TimelinePageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TherapyTimelinePage);

export default TimelinePageContainer;
