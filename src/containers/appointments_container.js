import { connect } from 'react-redux';
import Appointments from 'srcRoot/pages/appointments';
import { actions } from 'srcRoot/redux/appointments';
import { actions as linkedPageActions } from 'srcRoot/redux/linked_page';

export function mapStateToProps({ appointments }) {
  return {
    appointments,
  };
}

export function mapDispatchToProps(dispatch) {
  return {
    deleteAppointment: (data) => dispatch(actions.deleteAppointment(data)),
    setBackLink: (data) => dispatch(linkedPageActions.setBackLink(data)),
  };
}

const AppointmentsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Appointments);

export default AppointmentsContainer;
