import { connect } from 'react-redux';
import HappyPageLink from 'srcRoot/components/happy_page_link';

export function mapStateToProps({ happyItems }) {
  return {
    happyItems,
  };
}

export function mapDispatchToProps(dispatch) {
  return {};
}

const HappyPageLinkContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(HappyPageLink);

export default HappyPageLinkContainer;
