import { connect } from 'react-redux';
import LinkedPage from 'srcRoot/components/linked_page';
import { actions } from 'srcRoot/redux/top_nav';

export function mapStateToProps({ linkedPage }) {
  return {
    linkedPage,
  };
}

export function mapDispatchToProps(dispatch) {
  return {
    hideTopNavSeparator: () => dispatch(actions.hideSeparator()),
    showTopNavSeparator: () => dispatch(actions.showSeparator()),
  };
}

const LinkedPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LinkedPage);

export default LinkedPageContainer;
