import { connect } from 'react-redux';
import GoalsHistoryPage from 'srcRoot/pages/goals_history';

export function mapStateToProps({ goals }) {
  return {
    goals,
  };
}

export function mapDispatchToProps(dispatch) {
  return {};
}

const GoalsHistoryContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(GoalsHistoryPage);

export default GoalsHistoryContainer;
