import { connect } from 'react-redux';
import TherapistPage from 'srcRoot/pages/therapist';

export function mapStateToProps({ review }) {
  return {
    reviews: review.reviews,
  };
}

export function mapDispatchToProps(dispatch) {
  return {};
}

const TherapistPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TherapistPage);

export default TherapistPageContainer;
