import { connect } from 'react-redux';
import Alerts from 'srcRoot/sections/alerts';
import { actions } from 'srcRoot/redux/alerts';

export function mapStateToProps({ alerts }) {
  return {
    alerts,
  };
}

export function mapDispatchToProps(dispatch) {
  return {
    removeAlert: (id) => dispatch(actions.removeAlert(id)),
  };
}

const AlertsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Alerts);

export default AlertsContainer;
