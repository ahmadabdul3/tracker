import { connect } from 'react-redux';
import TherapistSchedulePage from 'srcRoot/pages/therapist_schedule';
import { actions as appointmentsActions } from 'srcRoot/redux/appointments';
import { actions as therapistScheduleActions } from 'srcRoot/redux/therapist_schedule';
import { actions as timelineActions } from 'srcRoot/redux/timeline';

export function mapStateToProps({ therapistSchedule }) {
  return {
    therapistSchedule,
  };
}

export function mapDispatchToProps(dispatch) {
  return {
    addAppointment: (data) => dispatch(appointmentsActions.addAppointment(data)),
    deleteTimeframe: (data) => dispatch(therapistScheduleActions.deleteTimeframe(data)),
    addEvent: (data) => dispatch(timelineActions.addEvent(data)),
  };
}

const TherapistSchedulePageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TherapistSchedulePage);

export default TherapistSchedulePageContainer;
