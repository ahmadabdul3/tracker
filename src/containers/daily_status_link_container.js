import { connect } from 'react-redux';
import DailyStatusLink from 'srcRoot/components/daily_status_link';

export function mapStateToProps({ dailyStatus }) {
  return {
    dailyStatus,
  };
}

export function mapDispatchToProps(dispatch) {
  return {};
}

const DailyStatusLinkContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(DailyStatusLink);

export default DailyStatusLinkContainer;
