import { connect } from 'react-redux';
import TopNav from 'srcRoot/sections/top_nav';
import { actions } from 'srcRoot/redux/app_menu';

export function mapStateToProps({ topNav }) {
  return {
    topNav,
  };
}

export function mapDispatchToProps(dispatch) {
  return {
    openAppMenu: () => dispatch(actions.openAppMenu()),
  };
}

const TopNavContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TopNav);

export default TopNavContainer;
