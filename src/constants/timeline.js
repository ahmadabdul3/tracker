export const timelineEventKeys = {
  STAGE_OF_CHANGE: 'stageOfChange',
  FEELING: 'feeling',
  APPOINTMENT: 'appointmentAdded',
  THOUGHTS: 'thoughts',
  GOALS: 'goals',
  HAPPY_ITEMS: 'happyItems',
  UNHAPPY_ITEMS: 'unhappyItems',
  ACTIVITY_ZONES: 'activityZones',
};

export const timelineEventLabels = {
  STAGE_OF_CHANGE: 'Stage of Change',
  FEELING: 'Feeling',
  THOUGHTS: 'Thoughts',
  APPOINTMENT: 'Appointment Added',
  GOALS: 'Added Goals',
  HAPPY_ITEMS: 'Added Happy Items',
  UNHAPPY_ITEMS: 'Added Unhappy Items',
  ACTIVITY_ZONES: 'Added Activity Zone',
}
