import React, { PropTypes, Component } from 'react';
import FormGroup from 'srcRoot/react_bb/form_group';
import CheckableInput from 'srcRoot/react_bb/checkable_input';
import { InstructableInput } from 'srcRoot/react_bb/instructable_input';
import { SelectableButtonFifty } from 'srcRoot/react_bb/selectable_button';
import { timelineEventKeys, timelineEventLabels } from 'srcRoot/constants/timeline';

export default class ThingsMakeMeUnhappy extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newButtons: [],
      unhappyItems: this.props.unhappyItems,
      addRequested: false,
      suggestions: [
        'Arguments', 'Work stress', 'Finances', 'Family',
        'School', 'stuff',
      ],
    }
  }

  addFromSuggestions = (value) => {
    const { suggestions } = this.state;
    this.addUnhappyItem(value);
    suggestions.splice(suggestions.indexOf(value), 1);
    this.setState({ suggestions });
  }

  addUnhappyItem = (value) => {
    const { unhappyItems } = this.state;
    if (unhappyItems.includes(value)) return;
    unhappyItems.push(value);
    this.props.addEvent({
      eventKey: timelineEventKeys.UNHAPPY_ITEMS,
      event: {
        label: timelineEventLabels.UNHAPPY_ITEMS,
        value: unhappyItems,
      },
    });
    this.setState({ unhappyItems, addRequested: false });
    this.props.updateUnhappyItems(unhappyItems);
  }

  deleteUnhappyItem = (id) => {
    const { unhappyItems } = this.state;
    unhappyItems.splice(id, 1);
    this.setState({ unhappyItems });
    this.props.updateUnhappyItems(unhappyItems);
  }

  requestAdd = () => {
    this.setState({ addRequested: true });
  }

  renderAddButton() {
    const { unhappyItems } = this.state;
    const buttonText = unhappyItems.length > 0 ? 'Add another Unhappy Item' : 'Add an Unhappy Item';

    return (
      <div className='centered-button-box'>
        <button className='primary-btn' onClick={this.requestAdd}>
          { buttonText }
        </button>
      </div>
    );
  }

  render() {
    const { unhappyItems, suggestions, addRequested } = this.state;

    return (
      <div>
        <div className='form-box'>
          Welcome to the unhappiness page. Here you can view and manage things that make you unhappy.
        </div>
        <FormGroup
          title='Things that make me unhappy'
          subtitle='You can have as many as you like'>

          <div className='addable-item-list'>
            <ul className='item-list'>
              {
               unhappyItems.length > 0 ? unhappyItems.map((item, key) => {
                  return (
                    <li className='list-item' key={key}>
                      <p className='text'>
                        { item }
                      </p>
                      <div className='icons'>
                        <i className="fa fa-times" aria-hidden="true" onClick={() => this.deleteUnhappyItem(key)}/>
                      </div>
                    </li>
                  );
                })
                : (
                  <li className='no-data-text'>
                    Looks like you don't have any unhappy items. Add some or select from the suggestions below
                  </li>
                )
              }
            </ul>
            <div className='add-section'>
              { addRequested ?
                (<CheckableInput checkboxClick={this.addUnhappyItem} />) :
                this.renderAddButton()
              }
            </div>
          </div>
        </FormGroup>
        <FormGroup title='Suggestions'>
          { suggestions && suggestions.length > 0 ? (<div className='even-50-btns-box'>
            { buttons(this.addFromSuggestions, unhappyItems, suggestions) }
          </div>) :
          <div className='no-data-text'>
            Sorry but we couldn't find any suggestions
          </div>}
        </FormGroup>
      </div>
    );
  }
};

function buttons(onClick, happyItems, suggestions) {
  return suggestions.map((thing, key) => {
    return (
      <button key={key} onClick={() => onClick(thing)} className='selectable-btn-50'>
        { thing }
      </button>
    );
  });
}
