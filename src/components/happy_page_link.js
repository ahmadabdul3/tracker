import React, { Component, PropTypes } from 'react';
import LinkBox from 'srcRoot/react_bb/link_box';

export default class HappyPageLink extends Component {
    constructor(props) {
      super(props);
    }

    render() {
      const { happyItems } = this.props;

      return (
        <LinkBox linkTo='/objectives/happy' title='What makes me happy'>
          { happyItems && happyItems.length > 0 ? <HappyItems data={happyItems} /> : <NoHappyItems />}
        </LinkBox>
      );
    }
}

function HappyItems({ data }) {
  return (
    <ul className='data-summary-list'>
    {
      data.map((happyItem, key) => {
        return (<li className='item' key={key}>{happyItem}</li>);
      })
    }
    </ul>
  );
}

function NoHappyItems() {
  return (
    <div className='no-data-text'>
      You dont have any happy items, click here to go add some
    </div>
  );
}
