import React, { PropTypes, Component } from 'react';

export default class ReviewableStars extends Component {
  static propTypes = {
    starClick: PropTypes.func,
  }

  constructor(props) {
    super(props);

    this.state = this.getInitialState();
  }

  getInitialState() {
    return {
      firstStarClass:  'fa-star-o',
      secondStarClass: 'fa-star-o',
      thirdStarClass: 'fa-star-o',
      fourthStarClass: 'fa-star-o',
      fifthStarClass: 'fa-star-o',
    };
  }

  fillInStars(numberOfStars) {
    this.props.starClick(numberOfStars);
    const stars = ['first', 'second', 'third', 'fourth', 'fifth'];
    const newState = this.getInitialState();

    for(let i = 0; i < numberOfStars; i++) {
      newState[stars[i] + 'StarClass'] = 'fa-star';
    }

    this.setState(newState);
  }

  render() {
    const {
      firstStarClass,
      secondStarClass,
      thirdStarClass,
      fourthStarClass,
      fifthStarClass,
    } = this.state;

    return (
      <div className='reviewable-stars'>
        <div className='star' onClick={() => this.fillInStars(1)}>
          <i className={`fa fa-lg ${firstStarClass}`} aria-hidden="true" />
        </div>
        <div className='star' onClick={() => this.fillInStars(2)}>
          <i className={`fa fa-lg ${secondStarClass}`} aria-hidden="true" />
        </div>
        <div className='star' onClick={() => this.fillInStars(3)}>
          <i className={`fa fa-lg ${thirdStarClass}`} aria-hidden="true" />
        </div>
        <div className='star' onClick={() => this.fillInStars(4)}>
          <i className={`fa fa-lg ${fourthStarClass}`} aria-hidden="true" />
        </div>
        <div className='star' onClick={() => this.fillInStars(5)}>
          <i className={`fa fa-lg ${fifthStarClass}`} aria-hidden="true" />
        </div>
      </div>
    );
  }
}
