import React, { Component, PropTypes } from 'react';
import { withRouter } from 'react-router';
import SearchableDropdown from 'srcRoot/react_bb/searchable_dropdown';
import DatePicker from 'srcRoot/react_bb/date_picker';
import DayPicker from 'srcRoot/react_bb/day_picker';
import FormGroup from 'srcRoot/react_bb/form_group';
import { InstructableInput } from 'srcRoot/react_bb/instructable_input';
import TimeBlockPicker from 'srcRoot/react_bb/time_block_picker';
import { timelineEventKeys, timelineEventLabels } from 'srcRoot/constants/timeline';

export default class ActivityZonesForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      addRequested: false,
    };

    this.resetNewActivityZone();
  }

  resetNewActivityZone() {
    this.newActivityZone = {
      activity: undefined,
      day: undefined,
      fromTime: undefined,
      toTime: undefined,
    }
  }

  updateActivity = (value) => {
    this.newActivityZone.activity = value;
  }
  updateDay = (value) => {
    this.newActivityZone.day = value;
  }
  updateFromTime = (value) => {
    this.newActivityZone.fromTime = value;
  }
  updateToTime = (value) => {
    this.newActivityZone.toTime = value;
  }

  requestAdd = () => {
    this.setState({ addRequested: true });
  }

  cancelAdd = () => {
    this.resetNewActivityZone();
    this.setState({ addRequested: false });
  }

  submit = (e) => {
    const { activity, day, fromTime, toTime } = this.newActivityZone;
    if (activity && day && fromTime && toTime) {
      this.props.addActivityZone(this.newActivityZone);
      this.props.addEvent({
        eventKey: timelineEventKeys.ACTIVITY_ZONES,
        event: {
          label: timelineEventLabels.ACTIVITY_ZONES,
          value: {
            activity,
            day,
            fromTime,
            toTime,
          },
        },
      });

      setTimeout(() => {
        this.props.alertInfo({ type: 'scheduledActivity', event: 'An activity has been automatically scheduled for you', activity, day, fromTime, toTime });
        this.props.scheduleActivity({ activity, day, fromTime, toTime });
        this.props.addNotification({ event: 'An activity has been automatically scheduled for you', activity, day, fromTime, toTime });
      }, 5000);

      this.cancelAdd();
    }
  }

  getCurrentActivityZones() {
    const { activityZones } = this.props;
    if (!activityZones || activityZones.length < 1) {
      return (
        <div className='form-box'>
          <span className='no-data-text'>
            You don't have any activity zones
          </span>
        </div>
      );
    }
    return activityZones.map((activityZone, key) => {
      return (<ActivityZone data={activityZone} key={key} />);
    });
  }

  getScheduledActivities() {
    const { scheduledActivities } = this.props;
    if (scheduledActivities && scheduledActivities.length > 0) {
      return scheduledActivities.map((scheduledActivity, key) => {
        return <ScheduledActivity data={scheduledActivity} key={key} />;
      });
    }
    return (
      <div className='form-box'>
        <span className='no-data-text'>
          You don't have any scheduled activities
        </span>
      </div>
    );
  }

  render() {
    const { activityZones } = this.props;
    const { addRequested } = this.state;

    return (
      <div>
        <div className='form-box'>
          Welcome to the activity zones page. Here you can create entries of possible times for you
          to perform an activity. The app will then automatically schedule an
          activity within the time frame you chose and notify you.
        </div>
        <div className='title-bar'>
          Scheduled Activities
        </div>
        { this.getScheduledActivities() }
        <div className='title-bar'>
          Activity Zones
        </div>
        { this.getCurrentActivityZones() }
        {
          addRequested ?
          (<FormGroup title='New activity zone'>
            <InstructableInput instructions='Select an activity'>
              <SearchableDropdown dropdownOptions={this.props.happyItems} onChange={this.updateActivity} />
            </InstructableInput>
            <InstructableInput instructions='Select a day'>
              <DayPicker onChange={this.updateDay}/>
            </InstructableInput>
            <InstructableInput instructions='Select a time zone'>
              <TimeBlockPicker onFromChange={this.updateFromTime} onToChange={this.updateToTime} />
            </InstructableInput>
            <div className='form-buttons'>
              <button className='btn' onClick={this.cancelAdd}>
                Cancel
              </button>
              <button className='submit-btn' onClick={this.submit}>
                Add
              </button>
            </div>
          </FormGroup>) : (<div className='centered-button-box'>
            <button className='primary-btn' onClick={this.requestAdd}>
              Add an Activity Zone
            </button>
          </div>)
        }
      </div>
    );
  }
}

function ActivityZone({ data }) {
  return (
    <div className='activity-zone'>
      <h3 className='title'>
        { data.activity }
      </h3>
      <h4 className='day'>
        <label className='label'>
          Day
        </label>
        { data.day }
      </h4>
      <div className='time-zone'>
        <label className='label'>
          Time Frame
        </label>
        <section className='from'>
          { data.fromTime.hour } { data.fromTime.periodOfDay }
        </section>
        <section className='to'>
          { data.toTime.hour } { data.toTime.periodOfDay }
        </section>
      </div>
    </div>
  );
}

function ScheduledActivity({ data }) {
  const { activity, day, fromTime } = data;
  const { hour, periodOfDay } = fromTime;
  return (
    <div className='scheduled-activity'>
      <h3 className='std-title'>
        { activity }
      </h3>
      <p className='date-time'>
        { day }, { hour } { periodOfDay }
      </p>
    </div>
  );
}
