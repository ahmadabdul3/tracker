import React, { PropTypes, Component } from 'react';
import FormGroup from 'srcRoot/react_bb/form_group';
import CheckableInput from 'srcRoot/react_bb/checkable_input';
import { InstructableInput } from 'srcRoot/react_bb/instructable_input';
import { SelectableButtonFifty } from 'srcRoot/react_bb/selectable_button';
import { timelineEventKeys, timelineEventLabels } from 'srcRoot/constants/timeline';

export default class ThingsMakeMeHappy extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newButtons: [],
      happyItems: this.props.happyItems,
      addRequested: false,
      suggestions: [
        'Exercise', 'Reading', 'Going on walks', 'Family',
        'Movies',
      ],
    }
  }

  addFromSuggestions = (value) => {
    const { suggestions } = this.state;
    this.addHappyItem(value);
    suggestions.splice(suggestions.indexOf(value), 1);
    this.setState({ suggestions });
  }

  addHappyItem = (value) => {
    const { happyItems } = this.state;
    if (happyItems.includes(value)) return;
    happyItems.push(value);
    this.props.addEvent({
      eventKey: timelineEventKeys.HAPPY_ITEMS,
      event: {
        label: timelineEventLabels.HAPPY_ITEMS,
        value: happyItems,
      },
    });
    this.setState({ happyItems, addRequested: false });
    this.props.updateHappyItems(happyItems);
  }

  deleteHappyItem = (id) => {
    const { happyItems } = this.state;
    happyItems.splice(id, 1);
    this.setState({ happyItems });
    this.props.updateHappyItems(happyItems);
  }

  requestAdd = () => {
    this.setState({ addRequested: true });
  }

  renderAddButton() {
    const { happyItems } = this.state;
    const buttonText = happyItems.length > 0 ? 'Add another Happy Item' : 'Add a Happy Item';

    return (
      <div className='centered-button-box'>
        <button className='primary-btn' onClick={this.requestAdd}>
          { buttonText }
        </button>
      </div>
    );
  }

  render() {
    const { happyItems, suggestions, addRequested } = this.state;

    return (
      <div>
        <div className='form-box'>
          Welcome to the happiness page. Here you can view and manage things that make you happy.
        </div>
        <FormGroup
          title='Things that make me happy'
          subtitle='You can have as many as you like'>

          <div className='addable-item-list'>
            <ul className='item-list'>
              {
                happyItems.length > 0 ? happyItems.map((item, key) => {
                  return (
                    <li className='list-item' key={key}>
                      <p className='text'>
                        { item }
                      </p>
                      <div className='icons'>
                        <i className="fa fa-times" aria-hidden="true" onClick={() => this.deleteHappyItem(key)}/>
                      </div>
                    </li>
                  );
                })
                : (
                  <li className='no-data-text'>
                    Looks like you don't have any happy items. Add some or select from the suggestions below
                  </li>
                )
              }
            </ul>
            <div className='add-section'>
              { addRequested ?
                (<CheckableInput checkboxClick={this.addHappyItem} />) :
                this.renderAddButton()
              }
            </div>

          </div>
        </FormGroup>
        <FormGroup title='Suggestions'>
          { suggestions && suggestions.length > 0 ? (<div className='even-50-btns-box'>
            { buttons(this.addFromSuggestions, happyItems, suggestions) }
          </div>) :
          <div className='no-data-text'>
            Sorry but we couldn't find any suggestions
          </div>}
        </FormGroup>
      </div>
    );
  }
};

function buttons(onClick, happyItems, suggestions) {
  return suggestions.map((thing, key) => {
    return (
      <button key={key} onClick={() => onClick(thing)} className='selectable-btn-50'>
        { thing }
      </button>
    );
  });
}
