import React, { Component, PropTypes } from 'react';
import LinkBox from 'srcRoot/react_bb/link_box';

export default class ActivityZonesPageLink extends Component {
    constructor(props) {
      super(props);
    }

    render() {
      const { goals } = this.props;

      return (
        <LinkBox linkTo='/objectives/activity-zones' title='Activity Zones'>
        </LinkBox>
      );
    }
}

// <p>
//   {`Plan activity zones and the app will automatically handle the scheduling for you.
//   You'll also get notifications every step along the way`}
// </p>
