import React, { Component, PropTypes } from 'react';
import LinkBox from 'srcRoot/react_bb/link_box';

export default class StageOfChangeLink extends Component {
    constructor(props) {
      super(props);
    }

    render() {
      return (
        <LinkBox linkTo='/stages-of-change' title='Stage of Change'>
          <div>
            4 (Action) - Im making changes
          </div>
        </LinkBox>
      );
    }
}

// <p>
//   {`Plan activity zones and the app will automatically handle the scheduling for you.
//   You'll also get notifications every step along the way`}
// </p>
