import React, { Component, PropTypes } from 'react';
import LinkBox from 'srcRoot/react_bb/link_box';

export default class DailyStatusLink extends Component {
    constructor(props) {
      super(props);
    }

    render() {
      const { dailyStatus } = this.props;

      return (
        <LinkBox linkTo='/daily-status' title='Daily Status'>
          { dailyStatus.submitted ? <DailyStatus data={dailyStatus} /> : <NoDailyStatus />}
        </LinkBox>
      );
    }
}

function DailyStatus({ data }) {
  return (
    <div className='daily-status'>
      <div className='feeling'>
        <label className='label'>
          Today I'm feeling
        </label>
        <div className='value'>
          { data.feeling }
        </div>
      </div>
      <p className='thoughts'>
        { data.thoughts }
      </p>
    </div>
  );
}

function NoDailyStatus() {
  return (
    <div className='no-data-text'>
      You havent filled out your daily status, click here to go fill it out
    </div>
  );
}
