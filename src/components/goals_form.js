import React, { PropTypes, Component } from 'react';
import FormGroup from 'srcRoot/react_bb/form_group';
import CheckableInput from 'srcRoot/react_bb/checkable_input';
import { InstructableInput } from 'srcRoot/react_bb/instructable_input';
import LinkBox from 'srcRoot/react_bb/link_box';
import { timelineEventKeys, timelineEventLabels } from 'srcRoot/constants/timeline';

const goalsSubtitle = `Add up to 3 goals that you want to accomplish`;
const additionalInstructions = `You can mark them as complete when you finish them`;

export default class GoalsForm extends Component {
  static PropTypes = {
    goals: PropTypes.array,
  };

  static defaultProps = {
    goals: [],
  };

  constructor(props) {
    super(props);

    this.state = {
      goals: props.goals,
      addRequested: false,
      suggestions: [
        'I want to read 2 books every month',
        'To spend more time with my family',
        'Eat less junk food and candy',
      ],
    }
  }

  requestAdd = () => {
    this.setState({ addRequested: true });
  }

  updateGoals(goals) {
    this.setState({ goals });
    this.props.updateGoals(goals);
  }

  addFromSuggestion = (value) => {
    const { suggestions } = this.state;
    const indexOfSuggestion = suggestions.indexOf(value);
    this.addGoal(value);
    suggestions.splice(indexOfSuggestion, 1);
    this.setState({ suggestions });
  }

  addGoal = (value) => {
    const { goals } = this.state;
    if (goals.length < 5) {
      goals.push({
        value,
        status: 'inProgress',
      });
      this.props.addEvent({
        eventKey: timelineEventKeys.GOALS,
        event: {
          label: timelineEventLabels.GOALS,
          value: goals,
        },
      });
      this.updateGoals(goals);
      this.setState({ addRequested: false });
    }
  }

  updateGoalStatus(id, status) {
    const { goals } = this.state;
    goals[id].status = status;
    this.updateGoals(goals);
  }

  deleteGoal = (id) => {
    this.updateGoalStatus(id, 'abandoned');
  }

  accomplishGoal = (id) => {
    this.updateGoalStatus(id, 'accomplished');
  }

  renderAddButton() {
    const { goals } = this.state;
    const buttonText = goals.length > 0 ? 'Add another Goal' : 'Add a Goal';
    if (goals.length < 5) {
      return (
        <div className='centered-button-box'>
          <button className='primary-btn' onClick={this.requestAdd}>
            { buttonText }
          </button>
        </div>
      );
    }

    return (
      <div className='no-data-text'>
        You can track more goals when you complete at least 1 currently in progress goal
      </div>
    )
  }

  render() {
    const { goals, suggestions, addRequested } = this.state;

    return (
      <div>
        <div className='form-box'>
          Welcome to the goals page. Here you can track, manage, and view a history of
          your goals. The app also finds and suggests goals from people similar to you.
        </div>
        <FormGroup title='In progress'
          subtitle="Goals you're currently tracking - 5 max"
          additionalInstructions="Come back and mark them as complete when they're done">

          <div className='addable-item-list'>
            <ul className='item-list'>
              {
                goals.length > 0 ? goals.map((item, key) => {
                  if (item.status === 'inProgress') {
                    return (
                      <li className='list-item' key={key}>
                        <p className='text'>
                          { item.value }
                        </p>
                        <div className='icons'>
                          <i className="fa fa-times" aria-hidden="true" onClick={() => this.deleteGoal(key)}/>
                          <i className="fa fa-flag-checkered" aria-hidden="true" onClick={() => this.accomplishGoal(key)} />
                        </div>
                      </li>
                    )
                  }
                  return null
                })
                : (
                  <li className='no-data-text'>
                    You don't have any goals in progress, add some or select from the suggestions below
                  </li>
                )
              }
            </ul>
            <div className='add-section'>
              { addRequested ?
                (<CheckableInput checkboxClick={this.addGoal} />) :
                this.renderAddButton()
              }
            </div>
          </div>
        </FormGroup>
        <FormGroup
          title='Suggestions'
          subtitle='Goals being tracked by people similar to you'
          additionalInstructions='Click one and we`ll automatically add it to your in progress goals'>
          <Suggestions goals={goals} suggestions={suggestions} onClick={this.addFromSuggestion} />
        </FormGroup>
        <LinkBox linkTo='/objectives/goals/history' title='History' />
      </div>
    );
  }
};

function Suggestions({ goals, suggestions, onClick }) {
  if (!suggestions || suggestions.length < 1) {
    return (
      <div className='no-data-text'>
        Bummer, we can't find any suggestions for you right now
      </div>
    )
  }
  return (
    <div className='btn-100-box'>
      { suggestions.map((suggestion, key) => {
        return goals.includes(suggestion) ? null :
        (<button className='btn' key={key} onClick={() => onClick(suggestion)}>
          { suggestion }
        </button>)
      })}
    </div>
  );
}
