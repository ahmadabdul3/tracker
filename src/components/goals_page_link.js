import React, { Component, PropTypes } from 'react';
import LinkBox from 'srcRoot/react_bb/link_box';

export default class GoalsPageLink extends Component {
    constructor(props) {
      super(props);
    }

    render() {
      const { goals } = this.props;

      return (
        <LinkBox linkTo='/objectives/goals' title='My Goals'>
          { goals && goals.length > 0 ? <Goals data={goals} /> : <NoGoals />}
        </LinkBox>
      );
    }
}

function Goals({ data }) {
  return (
    <ul className='data-summary-list'>
    {
      data.map((goal, key) => {
        if (goal.status === 'inProgress') {
          return (<li className='item' key={key}>{goal.value}</li>);
        }
        return null;
      })
    }
    </ul>
  );
}

function NoGoals() {
  return (
    <div className='no-data-text'>
      You dont have any goals, click here to go add some
    </div>
  );
}
