import React, { Component, PropTypes } from 'react';
import LinkBox from 'srcRoot/react_bb/link_box';

export default class UnhappyPageLink extends Component {
    constructor(props) {
      super(props);
    }

    render() {
      const { unhappyItems } = this.props;
      return (
        <LinkBox linkTo='/objectives/unhappy' title='What makes me unhappy'>
          { unhappyItems && unhappyItems.length > 0 ? <UnhappyItems data={unhappyItems} /> : <NoUnhappyItems />}
        </LinkBox>
      );
    }
}

function UnhappyItems({ data }) {
  return (
    <ul className='data-summary-list'>
    {
      data.map((unhappyItem, key) => {
        return (<li className='item' key={key}>{unhappyItem}</li>);
      })
    }
    </ul>
  );
}

function NoUnhappyItems() {
  return (
    <div className='no-data-text'>
      You dont have any unhappy items, click here to go add some
    </div>
  );
}
