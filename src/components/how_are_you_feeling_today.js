import React, { Component } from 'react';
import { InstructableInputGreen } from 'srcRoot/react_bb/instructable_input';
import { withRouter } from 'react-router';
import { timelineEventKeys, timelineEventLabels } from 'srcRoot/constants/timeline';

const title = `Share your day with us`;

class HowAreYouFeelingToday extends Component {
  constructor(props) {
    super(props);

    this.state = {
      feeling: '',
      thoughts: '',
      buttonOneClass: '',
      buttonTwoClass: '',
      buttonThreeClass: '',
    }
  }

  setSelected = (e) => {
    const button = e.target;
    const buttonName = button.name;
    const value = button.value;
    const newState = {
      feeling: value,
      buttonOneClass: '',
      buttonTwoClass: '',
      buttonThreeClass: '',
    };
    newState[buttonName + 'Class'] = 'selected';

    this.setState(newState);
  }

  updateThoughts = (e) => {
    const value = e.target.value;
    this.setState({ thoughts: value });
  }

  submit = () => {
    const { feeling, thoughts } = this.state;

    if (feeling && thoughts) {
      const { submitDailyStatus, router } = this.props;
      submitDailyStatus({ feeling, thoughts });
      this.props.addEvent({
        eventKey: timelineEventKeys.FEELING,
        event: {
          label: timelineEventLabels.FEELING,
          value: feeling,
        },
      });
      this.props.addEvent({
        eventKey: timelineEventKeys.THOUGHTS,
        event: {
          label: timelineEventLabels.THOUGHTS,
          value: thoughts,
        },
      });
      router.push('/');
    }
  }

  render() {
    const { buttonOneClass, buttonTwoClass, buttonThreeClass } = this.state;

    return (
      <div className='how-are-you-feeling-today'>
          <InstructableInputGreen instructions='How are you feeling today?'>
            <div className='three-hrz-btns-box'>
              <button className={`btn ${buttonOneClass}`} name='buttonOne' value='positive' onClick={this.setSelected}>
                Positive
              </button>
              <button className={`btn ${buttonTwoClass}`} name='buttonTwo' value='neutral' onClick={this.setSelected}>
                Neutral
              </button>
              <button className={`btn ${buttonThreeClass}`} name='buttonThree' value='negative' onClick={this.setSelected}>
                Negative
              </button>
            </div>
          </InstructableInputGreen>
          <InstructableInputGreen
            instructions='Share your thoughts'
            secondaryInstructions={`You can say anything here like "Work was stressful" or "Looking forward to lunch with my sister"`}>
            <div className='status-update'>
              <textarea className='input' onChange={this.updateThoughts} />
            </div>
          </InstructableInputGreen>
          <div className='form-buttons'>
            <button className='submit-btn' onClick={this.submit}>
              Share
            </button>
          </div>
      </div>
    );
  }
}

export default withRouter(HowAreYouFeelingToday);
