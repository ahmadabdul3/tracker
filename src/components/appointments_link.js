import React, { Component, PropTypes } from 'react';
import LinkBox from 'srcRoot/react_bb/link_box';
import AppointmentSummary from 'srcRoot/react_bb/appointment_summary';
import { Link } from 'react-router';

const noDataText = `You don't have any appointments scheduled right now`;

export default class AppointmentsLink extends Component {
    constructor(props) {
      super(props);
    }

    render() {
      const { appointments } = this.props;
      
      return (
        <LinkBox linkTo='/appointments' title='Appointments'>
          {
            appointments && appointments.length > 0 ? <AppointmentSummary data={appointments[0]} /> : <NoAppointmentsMessage />
          }
        </LinkBox>
      );
    }
}

function NoAppointmentsMessage() {
  return (
    <div className='no-data-text'>
      You don't have any upcoming appointments scheduled
    </div>
  );
}

// <p className='no-data-text'>
//   { noDataText }
// </p>
