import React, { PropTypes, Component } from 'react';
import { Link } from 'react-router';

/**

  - each linked page should be given a default backlink when it's created

  - there are special circumstances that require a backlink to be stored
    in the redux store

  - an example of such a scenario is going from one linked-page to another linked-page
    directly and bypassing the app navigation

  - in these situations, we want the back button to go back to the initial linked-page,
    which then goes back to it's default back link

  - a specific example is patient appointments; clicking on 'add more' brings them
    directly to their therapist's schedule, after selecting appointments, when they click back
    they should be taken to their appointments again (instead of the default /therapist page)

  - the onLeave event on the react-router will handle clearing the global backlink

  - initially it was in this component, and was done when a person clicks back, but the issue
    with that is if the person doesnt click back, but instead uses another navigation method.
    When they go to another 'linked-page' the global backlink will still be there and will bring
    them back to the global backlink. So now it's done on leave of the route

  - maybe we should do this on leave of every route in the router? we shall see

*/

export default class LinkedPage extends Component {
  static propTypes = {
    backLink: PropTypes.string,
    children: PropTypes.node,
    linkedPage: PropTypes.object,
    title: PropTypes.string,
  }

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.hideTopNavSeparator();
  }

  componentWillUnmount() {
    this.props.showTopNavSeparator();
  }

  render() {
    const { children, title, linkedPage, backLink } = this.props;
    const pathTo = (linkedPage && linkedPage.backLink) ? linkedPage.backLink : backLink;

    return (
      <div className='linked-page'>
        <nav className='navigation'>
          <Link to={pathTo} className='nav-item link'>
            <i className="fa fa-caret-left" aria-hidden="true" />
            Back
          </Link>
          {title ? (<h2 className='title'>{title}</h2>) : null}
        </nav>
        <div className='page-content'>
          { children }
        </div>
      </div>
    );
  }
}
