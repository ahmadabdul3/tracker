import React from 'react';
import { render } from 'react-dom'
import { Router, Route, IndexRoute, hashHistory } from 'react-router'
import TopNavContainer from 'srcRoot/containers/top_nav_container';
import BottomNav from 'srcRoot/sections/bottom_nav';
import HomePage from 'srcRoot/pages/home';
import NotificationsContainer from 'srcRoot/containers/notifications_container';
import TherapistPageContainer from 'srcRoot/containers/therapist';
import TimelinePageContainer from 'srcRoot/containers/timeline_page_container';
import ObjectivePage from 'srcRoot/pages/objective_page';
import TherapistReviewContainer from 'srcRoot/containers/review';
import GoalsPage from 'srcRoot/pages/goals_page';
import HappyPage from 'srcRoot/pages/happy_page';
import UnhappyPage from 'srcRoot/pages/unhappy_page';
import DailyStatus from 'srcRoot/pages/daily_status';
import ActivityZonesPage from 'srcRoot/pages/activity_zones_page';
import AppointmentsContainer from 'srcRoot/containers/appointments_container';
import AppMenuContainer from 'srcRoot/containers/app_menu_container';
import ChatPageContainer from 'srcRoot/containers/chat_page_container';
import TherapistSchedulePageContainer from 'srcRoot/containers/therapist_schedule_container';
import GoalsHistoryContainer from 'srcRoot/containers/goals_history_container';
import LoginPageContainer from 'srcRoot/containers/login_page_container';
import AlertsContainer from 'srcRoot/containers/alerts_container';
import StagesOfChangePage from 'srcRoot/pages/stages_of_change_page';
import SignUpPageContainer from 'srcRoot/containers/sign_up_container';

import { actions as linkedPageActions } from 'srcRoot/redux/linked_page';
import { actions as topNavActions } from 'srcRoot/redux/top_nav';

export default function MainRouter({ store }) {

  // see linked-page component for an explanation of what this is about
  function onLeaveTherapistSchedule() {
    store.dispatch(linkedPageActions.clearBackLink());
  }

  function onHomePageEnter() {
    store.dispatch(topNavActions.hideBackgroundColor());
  }

  function onHomePageLeave() {
    store.dispatch(topNavActions.showBackgroundColor());
  }

  function authenticate(nextState, replaceState) {
    const state = store.getState();
    if (!state.login.username) {
      replaceState({ nextPathname: nextState.location.pathname }, '/login');
    }
  }

  return (
    <Router history={hashHistory}>
      <Route path='/login' component={LoginPageContainer} />
      <Route path='/signup' component={SignUpPageContainer} />
      <Route component={layout}>
        <Route path='/' component={HomePage} onEnter={onHomePageEnter} onLeave={onHomePageLeave}/>
        <Route path='/'>
          <Route path='daily-status' component={DailyStatus} />
          <Route path='appointments' component={AppointmentsContainer} />
          <Route path='stages-of-change' component={StagesOfChangePage} />
        </Route>
        <Route path='/notifications' component={NotificationsContainer} />
        <Route path='/chat' component={ChatPageContainer} />
        <Route path='/therapist' component={TherapistPageContainer} />
        <Route path='/therapist'>
          <Route path='review' component={TherapistReviewContainer} />
          <Route path='schedule' component={TherapistSchedulePageContainer} onLeave={onLeaveTherapistSchedule} />
        </Route>
        <Route path='/timeline' component={TimelinePageContainer} />
        <Route path='/objectives' component={ObjectivePage} />
        <Route path='/objectives'>
          <Route path='goals' component={GoalsPage} />
          <Route path='goals'>
            <Route path='history' component={GoalsHistoryContainer} />
          </Route>
          <Route path='happy' component={HappyPage} />
          <Route path='unhappy' component={UnhappyPage} />
          <Route path='activity-zones' component={ActivityZonesPage} />
        </Route>
      </Route>
    </Router>
  );
}

function layout({ children }) {
  return (
    <div className='app'>
      <div className='nav-size-space' />
      <TopNavContainer />
      <AppMenuContainer />
      <AlertsContainer />
      <BottomNav />
      <section className='body'>
        { children }
      </section>
    </div>
  );
}
