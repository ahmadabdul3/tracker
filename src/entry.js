import React, { Component } from 'react';
import ReactDom from 'react-dom';
import MainRouter from 'srcRoot/router/router';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from 'srcRoot/redux';

const initialState = {};
const store = createStore(reducer, initialState);

if (typeof window !== 'undefined') {
  ReactDom.render(
    <Provider store={store}>
      <MainRouter store={store} />
    </Provider>,
    document.getElementById('entry-point')
  );
}
