import React, { Component } from 'react';
import { timelineEventKeys, timelineEventLabels } from 'srcRoot/constants/timeline';

export default class AvailabilityEntry extends Component {
  constructor(props) {
    super(props);
  }

  /**

    timeframe looks like this:
    {
      startTime: { hour, periodOfDay},
      endTime: { hour, periodOfDay }
    }

  */
  onButtonClick = (timeframe) => {
    const { monthId, data } = this.props;
    const { id, day, number, month, year } = data;
    const dayId = id;
    const appointmentData = {
      id,
      day,
      number,
      month,
      year,
      timeframe,
    };
    this.props.addEvent({
      eventKey: timelineEventKeys.APPOINTMENT,
      event: {
        label: timelineEventLabels.APPOINTMENT,
        value: appointmentData,
      },
    });
    this.props.addAppointment(appointmentData);
    this.props.deleteTimeframe({ monthId, dayId: id, timeframeId: timeframe.id });
  }

  render() {
    const { addAppointment, deleteTimeframe, data } = this.props;
    const { availableTimeframes } = data;

    return (
      <div className='availability-entry'>
        <div className='day-number'>
          <div className='number'>
            { data.number }
          </div>
          <div className='day'>
            { data.day.substring(0, 3) }
          </div>
        </div>
        <div className='details'>
          <header className='header'>
            <h3 className='title'>
              Available Times
            </h3>
            <p>
              Click a time block to reserve it
            </p>
          </header>

          <div className='btn-100-box'>
            {
              availableTimeframes && availableTimeframes.length > 0 ? availableTimeframes.map((timeframe, key) => {
                const startTime = timeframe.startTime;
                const endTime = timeframe.endTime;
                return (
                  <button className='btn' onClick={() => this.onButtonClick(timeframe)} key={key}>
                    {startTime.hour} {startTime.periodOfDay} - {endTime.hour} {endTime.periodOfDay}
                  </button>
                )
              }) : <div className='no-data-text'>There are no more available times on this day</div>
            }
          </div>
        </div>
      </div>
    );
  }
}

function structureAppointmentData(data, timeframe) {
  const { id, day, number, month, year } = data;
  const returndata = {
    id,
    day,
    number,
    month,
    year,
    timeframe,
  }
  return returndata;
}

/**


{
  id: 1,
  day: 'Monday',
  number: 10,
  month: 'December',
  year: 2017,
  availableTimeframes: [
    {
      startTime: {
        hour: '5:00',
        periodOfDay: 'pm',
      },
      endTime: {
        hour: '5:25',
        periodOfDay: 'pm',
      }
    },
    {
      startTime: {
        hour: '5:30',
        periodOfDay: 'pm',
      },
      endTime: {
        hour: '5:25',
        periodOfDay: 'pm',
      }
    }
  ]
},

*/
