import React, { PropTypes, Component } from 'react';
import FormGroup from 'srcRoot/react_bb/form_group';
import CheckableInput from 'srcRoot/react_bb/checkable_input';
import { InstructableInput } from 'srcRoot/react_bb/instructable_input';
import { SelectableButtonFifty } from 'srcRoot/react_bb/selectable_button';

const ButtonListRendererForm = ({
  addAction,
  formTitle,
  formSubtitle,
  formAdditionalInstructions,
  inputTitle,
  initialList,
  newList
}) => {
  return (
    <FormGroup
      title={formTitle}
      subtitle={formSubtitle}
      additionalInstructions={formAdditionalInstructions}>
      <div className='even-50-btns-box'>
        { initialList }
      </div>
      <InstructableInput instructions={inputTitle}>
        <CheckableInput checkboxClick={addAction} />
      </InstructableInput>
      <div className='list-box'>
        <div className='list-items-box even-50-btns-box'>
          {
            newList.length > 0 ? newList.map((item, key) => {
              return (
                <SelectableButtonFifty key={key} text={item} selected />
              );
            })
            : ''
          }
        </div>
      </div>
    </FormGroup>
  );
};

ButtonListRendererForm.propTypes = {
  addAction: PropTypes.func,
  formTitle: PropTypes.string,
  formSubtitle: PropTypes.string,
  formAdditionalInstructions: PropTypes.string,
  inputTitle: PropTypes.string,
  initialList: PropTypes.array,
  newList: PropTypes.array,
};

export default ButtonListRendererForm;
