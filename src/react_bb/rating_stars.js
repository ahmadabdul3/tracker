import React, { PropTypes, Component } from 'react';

export default class RatingStars extends Component {
  static defaultProps = {
    starRating: 0,
  }

  constructor(props) {
    super(props);

    this.state = {
      starClasses: this.getStarClasses(),
    };
  }

  getStarClasses() {
    const { starRating } = this.props;
    const emptyStarClass = 'fa-star-o';
    const filledStarClass = 'fa-star';
    const starClasses = [];

    for(let i = 0; i < 5; i++) {
      if (i < starRating) {
        starClasses.push(filledStarClass);
      } else {
        starClasses.push(emptyStarClass);
      }
    }

    return starClasses;
  }

  render() {
    const { starClasses } = this.state;

    return (
      <div className='rating-stars'>
        <div className='star'>
          <i className={`fa fa-lg ${starClasses[0]}`} aria-hidden="true" />
        </div>
        <div className='star'>
          <i className={`fa fa-lg ${starClasses[1]}`} aria-hidden="true" />
        </div>
        <div className='star'>
          <i className={`fa fa-lg ${starClasses[2]}`} aria-hidden="true" />
        </div>
        <div className='star'>
          <i className={`fa fa-lg ${starClasses[3]}`} aria-hidden="true" />
        </div>
        <div className='star'>
          <i className={`fa fa-lg ${starClasses[4]}`} aria-hidden="true" />
        </div>
      </div>
    );
  }
}
