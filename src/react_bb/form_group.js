import React, { PropTypes } from 'react';

const FormGroup = ({ children, title, subtitle, additionalInstructions }) => (
  <div className='form-group'>
    <div className='header'>
      {
        title ?
        <h4 className='title'>
          { title }
        </h4> : ''
      }
      {
        subtitle ?
        <h5 className='subtitle'>
          { subtitle }
        </h5> : ''
      }
      {
        additionalInstructions ?
        <p className='additional-instructions'>
          { additionalInstructions }
        </p> : ''
      }
    </div>
    <div className='inputs'>
      { children }
    </div>
  </div>
);

FormGroup.propTypes = {
  additionalInstructions: PropTypes.string,
  children: PropTypes.node,
  subtitle: PropTypes.string,
  title: PropTypes.string,
};

export default FormGroup;
