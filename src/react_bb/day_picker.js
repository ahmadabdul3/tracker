import React, { Component } from 'react';
import SearchableDropdown from 'srcRoot/react_bb/searchable_dropdown';

export default class DayPicker extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { onChange } = this.props;
    return(
      <div className='day-picker'>
        <SearchableDropdown dropdownOptions={weekDays()} onChange={onChange} />
      </div>
    );
  }
}

function weekDays() {
  return [
    'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday',
  ]
}
