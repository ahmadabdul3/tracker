import React, { PropTypes, Component } from 'react';

export default class SelectableButton extends Component {
  static propTypes = {
    text: PropTypes.string,
    buttonType: PropTypes.string,
    selectedIcon: PropTypes.string,
    selected: PropTypes.bool,
  };

  static defaultProps = {
    selected: false,
  }

  constructor(props) {
    super(props);

    this.state = {
      selected: props.selected,
    }
  }

  select = () => {
    this.setState({ selected: !this.state.selected });
    this.props.onClick(this.props.text);
  }

  render() {
    const { buttonType, text, selectedIcon } = this.props;
    const { selected } = this.state;
    const buttonClass = `${BUTTON_TYPES[buttonType]} ${selected ? 'selected' : ''}`;

    return (
      <button className={buttonClass} onClick={this.select}>
        { text }
      </button>
    );
  }
}

export const SelectableButtonFifty = ({ text, selected, onClick }) => {
  return (
    <SelectableButton buttonType='buttonFifty' text={text} selected={selected} onClick={onClick} />
  );
}


export const BUTTON_TYPES = {
  buttonFifty: 'selectable-btn-50',
};
