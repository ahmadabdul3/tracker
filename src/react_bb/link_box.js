import React, { PropTypes } from 'react';
import { Link } from 'react-router';

const LinkBox = ({ linkTo, children, iconWord, title }) => {
  return (
    <Link to={linkTo} className='link-box'>
      <div className='info'>
        <h2 className='std-title'>
          { title }
        </h2>
        { children ? <div className='children'>{children}</div> : null }
      </div>
      <div className='icon'>
        { iconWord ? (<div className='icon-word'>{ iconWord }</div>) : null }
        <i className="fa fa-caret-right" aria-hidden="true" />
      </div>
    </Link>
  );
};

LinkBox.propTypes = {
  children: PropTypes.node,
  iconWord: PropTypes.string,
  linkTo: PropTypes.string,
  title: PropTypes.string,
};

export default LinkBox;
