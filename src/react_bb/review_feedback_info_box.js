import React from 'react';
import RatingStars from 'srcRoot/react_bb/rating_stars';

const defaultTitle = 'He really cares!';
const defaultText = `Really helped with has populo consequuntur definitiones te.
Aliquip principes sea id. Qui ne tamquam temporibus cotidieque, at per
omnis aliquam, enim aliquando sea ex.`;

const ReviewFeedbackInfoBox = ({ title, body, starRating }) => (
  <div className='review-feedback-info-box'>
    <h3 className='content title'>
      { title || defaultTitle }
    </h3>
    <div className='content'>
      <RatingStars starRating={starRating || 5} />
    </div>
    <p className='content body'>
      { body || defaultText }
    </p>
  </div>
);

export default ReviewFeedbackInfoBox;
