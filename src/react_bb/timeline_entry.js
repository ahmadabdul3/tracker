import React from 'react';
import TwoPersonIcon from 'srcRoot/react_bb/two_person_icon';
import { timelineEventKeys } from 'srcRoot/constants/timeline';

export default function TimelineEntry({ entry, appointments }) {
  return (
    <div className='timeline-entry'>
      <div className='date'>
        <div className='timeline-location-dot' />
        <div>
          <div className='number'>
            { entry.number }
          </div>
          <div className='day'>
            { entry.day }
          </div>
        </div>
      </div>
      <div className='info'>
        {
          Object.keys(entry.events).map((eventKey, key) => {
            const event = entry.events[eventKey];
            return (
              <div className='labeled-data' key={key}>
                <div className='title'>
                  { event.label }
                </div>
                <div className='data'>
                  { formatEventValue(event.value, eventKey, appointments) }
                </div>
              </div>
            )
          })
        }
      </div>
    </div>
  );
}

function formatEventValue(eventValue, eventKey, appointments) {
  switch (eventKey) {
    case timelineEventKeys.HAPPY_ITEMS:
    case timelineEventKeys.UNHAPPY_ITEMS:
      return horizontalListValueFormatter(eventValue);

    case timelineEventKeys.GOALS:
      return goalValueFormatter(eventValue);

    case timelineEventKeys.ACTIVITY_ZONES:
      return activityZoneValueFormatter(eventValue);

    case timelineEventKeys.APPOINTMENT:
      return appointmentValueFormatter(appointments);

    default:
      return eventValue;
  }
}

function horizontalListValueFormatter(eventValue) {
  return eventValue.join(', ');
}

function verticalListValueFormetter(eventValue) {
  return eventValue.map((value, key) => {
    return (
      <div key={key}>
        - {value}
      </div>
    );
  });
}

function goalValueFormatter(eventValue) {
  return eventValue.map((value, key) => {
    return (
      <div key={key}>
        - { value.value }
      </div>
    );
  })
}

function activityZoneValueFormatter(eventValue) {
  const { activity, day, fromTime, toTime } = eventValue;
  return (
    <div>
      <span>{ activity } - { day }, </span>
      <span>{ fromTime.hour } { fromTime.periodOfDay } - </span>
      <span>{ toTime.hour } { toTime.periodOfDay }</span>
    </div>
  );
}

function appointmentValueFormatter(appointments) {
  return appointments.map((appointment, key) => {
    const { day, month, number, timeframe } = appointment;
    const { startTime } = timeframe;
    return (
      <div key={key}>
        <span>Eric Shafer - {day.substring(0, 3)} {month.substring(0, 3)} {number}, </span>
        <span>{ startTime.hour } { startTime.periodOfDay }</span>
      </div>
    );
  });
}
