import React, { Component } from 'react';
import SearchableDropdown from 'srcRoot/react_bb/searchable_dropdown';

export default class DatePicker extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div className='date-picker'>
        <SearchableDropdown dropdownOptions={months()} />
        <SearchableDropdown />
        <SearchableDropdown dropdownOptions={years()}/>
      </div>
    );
  }
}

function months() {
  return [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ]
}

function years() {
  const thisYear = new Date().getFullYear();
  const yearsList = [];
  for(let i = 0; i < 150; i++) {
    yearsList.push(thisYear - i);
  }
  return yearsList;
}
