import React, { Component, PropTypes } from 'react';

const noOptions = `No results`;

export default class SearchableDropdown extends Component {

  /**

  - the name prop can be passed for debugging purposes
    on pages with many dropdowns

  - it will allow us to identify which logs belong to which dropdownOptions

  */
  static propTypes = {
    dropdownOptions: PropTypes.array,
    name: PropTypes.string,
    onChange: PropTypes.func,
  }

  constructor(props) {
    super(props);

    this.state = {
      inputFocused: false,
      filterOptions: false,
      searchValue: undefined,
      inputValue: '',
    }

    this.blurTimeoutId = null;
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.hideDropdown);
  }

  /**

  - hide happens on click rather than on input blur

  - this is because on touch devices, you need to be able
    to scroll through dropdown options without closing them

  - on touch devices, you can only scroll by touching
    so touching the dropdown to scroll will blur away from the input
    which would close the dropdown

  */
  hideDropdown = (e) => {
    /**

    - setting filterOptions to false here is causing all the options
      to appear when hiding the dropdown menu.

    - this is a weird visual effect because if we search for a value
      and the dropdown filters for it, we end up with 1 dropdown option

      but then when filterOptions is set to false, the dropdown is re-populated
      so it grows to fit all options as its collapsing

    - need to fix it eventually, but at the moment, ain't nobody got time fo' dat

    */
    this.setState({ inputFocused: false, filterOptions: false });
    document.removeEventListener('click', this.hideDropdown);
  }

  onInputFocus = () => {
    this.setState({ inputFocused: true });
    setTimeout(() => {
      document.addEventListener('click', this.hideDropdown);
    }, 50);
  }

  onInputChange = (e) => {
    let searchValue = e.target.value;
    this.setState({ searchValue, inputValue: searchValue, filterOptions: true });
    this.props.onChange(searchValue.trim());
  }

  optionClick = (inputValue) => {
    this.setState({ inputValue, searchValue: inputValue });
    this.props.onChange(inputValue);
  }

  filterOptionsBySearch(list) {
    const { searchValue } = this.state;

    if (searchValue) {
      return list.filter(value => {
        return value.toUpperCase().indexOf(searchValue.toUpperCase()) > -1;
      });
    }

    return list;
  }

  getOptions() {
    const { dropdownOptions } = this.props;
    const { filterOptions } = this.state;
    let options = dropdownOptions ? dropdownOptions : [];
    if (filterOptions) options = this.filterOptionsBySearch(options);

    if (options.length < 1) return (<div className='option'>{noOptions}</div>);

    return options.map((option, key) => {
      return (
        <div className='option' key={key} onClick={() => this.optionClick(option)}>
          {option}
        </div>
      )
    });
  }

  render() {
    const { inputFocused, inputValue } = this.state;
    const dropdownClass = inputFocused ? 'expanded' : '';

    return (
      <div className='searchable-dropdown'>
        <input
          className='input'
          onFocus={this.onInputFocus}
          onChange={this.onInputChange}
          value={inputValue} />
        <div className={`dropdown ${dropdownClass}`}>
          { this.getOptions() }
        </div>
      </div>
    );
  }
}
