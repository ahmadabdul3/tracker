import React from 'react';

const PageVisibility = ({ visible, children }) => (
  <div className={ visible ? 'page-visibility' : 'page-visibility hidden' }>
    { children }
  </div>
);

export default PageVisibility;
