import React, { PropTypes } from 'react';

const LabeledData = ({ label, data }) => {
  return (
    <div className='labeled-data'>
      <label className='title'>
        { label }
      </label>
      <div className='data'>
        { data }
      </div>
    </div>
  );
};

LabeledData.propTypes = {
  data: PropTypes.string,
  label: PropTypes.string,
}

export default LabeledData;
