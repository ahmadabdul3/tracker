import React from 'react';

const therapistImageDefault = 'http://zt-four.zoodemo.com/images/article/mark3.jpg';

export default function AppointmentSummary({ deletable, deleteAppointment, data }) {
  const { timeframe } = data;
  return (
    <div className='appointment-summary'>
      { deletable ? <i className='fa fa-times delete-icon' onClick={() => deleteAppointment(data)}/> : null }
      <div className='photo-box'>
        <img className='photo' src={ therapistImageDefault } />
      </div>
      <div className='info'>
        <h3 className='title'>
          Eric Shafer
        </h3>
        <div className='detail-entry'>
          <span className='label'>
            Date
          </span>
          <span className='value'>
            {data.month} {data.number}, {data.year}
          </span>
        </div>
        <div className='detail-entry'>
          <span className='label'>
            Day
          </span>
          <span className='value'>
            {data.day}
          </span>
        </div>
        <div className='detail-entry'>
          <span className='label'>
            Time
          </span>
          <span className='value'>
            {timeframe.startTime.hour} {timeframe.startTime.periodOfDay}
          </span>
        </div>
      </div>
    </div>
  );
}

// return {
//   id,
//   day,
//   number,
//   month,
//   year,
//   startTime: {
//    hour, periodOfDay
//   }
//   endTime: {
//    hour, periodOfDay
//   }
// };
