import React from 'react';
import { Link } from 'react-router';

const NavItem = ({ children, page }) => (
  <Link to={`/${page}`} activeClassName='selected' className='nav-item'>
    <div className='content'>
        <div className='icon-box'>
          { children }
        </div><div className='v-m-hack'></div>
    </div>
  </Link>
);

export default NavItem;
