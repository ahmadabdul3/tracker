import React, { PropTypes, Component } from 'react';

export default class CheckableInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: '',
    }
  }

  checkboxClick = () => {
    if (this.state.inputValue.trim()) {
      this.props.checkboxClick(this.state.inputValue);
      this.setState({ inputValue: '' })
    }
  }

  getCheckedClass() {
    if (this.state.inputValue.trim()) return 'active';
    return '';
  }

  change = (e) => {
    const inputValue = e.target.value;
    this.setState({ inputValue });
  }

  render() {
    const checkedClass = this.getCheckedClass();
    const { children } = this.props;

    return (
      <div className='checkable-input'>
        <div className='input-box'>
          <input
            type='text'
            value={this.state.inputValue}
            className='input'
            onChange={this.change} />
        </div>
        <div className={`checkbox ${checkedClass}`} onClick={this.checkboxClick}>
          <i className="fa fa-lg fa-check" aria-hidden="true" />
        </div>
      </div>
    );
  }
}
