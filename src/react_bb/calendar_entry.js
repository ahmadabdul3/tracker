import React from 'react';
import { Link } from 'react-router';

const therapistImageDefault = 'http://zt-four.zoodemo.com/images/article/mark3.jpg';
const dayNumDefault = '14';
const dayTextDefault = 'Tue';
const therapistNameDefault = 'Justin Banter';
const timeLeftDefault = '1d, 2h, 5m';
const eventTitleDefault = 'Some event name';

const CalendarEntry = ({ dayNum, dayText, eventTitle, therapistName, therapistImage, timeLeft }) => (
  <div className='calendar-entry'>
    <div className='day'>
      <div className='number'>
        { dayNum || dayNumDefault }
      </div>
      <div className='text'>
        { dayText || dayTextDefault }
      </div>
    </div>
    <div className='details'>
      <div className='component-icon gen-abs-content therapist-image'>
        <Link className='image-container-xsm' to='/therapist'>
          <img className='img' src={ therapistImage || therapistImageDefault } />
        </Link>
      </div>
      <div className='content'>
        <h4 className='title'>
          { eventTitle || eventTitleDefault }
        </h4>
        <div className='info-items'>
          <div className='info-item'>
            <label className='therapist-name'>
              { therapistName || therapistNameDefault }
            </label>
          </div>
          <div className='info-item'>
            <div className='seperator-dot'></div>
          </div>
          <div className="info-item">
            <span className='text vertical-center'>
              { timeLeft || timeLeftDefault }
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default CalendarEntry;
