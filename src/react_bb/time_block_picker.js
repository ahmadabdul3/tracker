import React, { Component } from 'react';
import SearchableDropdown from 'srcRoot/react_bb/searchable_dropdown';
import { InstructableInput } from 'srcRoot/react_bb/instructable_input';

export default class TimeBlockPicker extends Component {
  constructor(props) {
    super(props);

    this.fromTime = {
      hour: undefined,
      periodOfDay: undefined,
    }

    this.toTime = {
      hour: undefined,
      periodOfDay: undefined,
    }
  }

  getNewTime(hour, periodOfDay) {
    return { hour, periodOfDay };
  }

  updateFromHour = (value) => {
    this.fromTime = this.getNewTime(value, this.fromTime.periodOfDay);
    let fromTime;
    // if both the period of day and the hour have values
    // then we send back the actual object
    // otherwise its undefined
    if (this.fromTime.periodOfDay && value) {
      fromTime = this.fromTime;
    }

    this.props.onFromChange(fromTime);
  }
  updateFromPeriod = (value) => {
    this.fromTime = this.getNewTime(this.fromTime.hour, value);
    let fromTime;
    if (this.fromTime.hour && value) {
      fromTime = this.fromTime;
    }

    this.props.onFromChange(fromTime);
  }

  updateToHour = (value) => {
    this.toTime = this.getNewTime(value, this.toTime.periodOfDay);
    let toTime;
    if (this.toTime.periodOfDay && value) {
      toTime = this.toTime;
    }

    this.props.onToChange(toTime);
  }
  updateToPeriod = (value) => {
    this.toTime = this.getNewTime(this.toTime.hour, value);
    let toTime;
    if (this.toTime.hour && value) {
      toTime = this.toTime;
    }

    this.props.onToChange(toTime);
  }


  render() {
    return(
      <div className='time-block-picker'>
        <div className='from'>
          <InstructableInput instructions='From'>
            <SearchableDropdown dropdownOptions={hours()} onChange={this.updateFromHour} />
            <SearchableDropdown dropdownOptions={timeOfDay()} onChange={this.updateFromPeriod} />
          </InstructableInput>
        </div>
        <div className='to'>
        <InstructableInput instructions='To'>
          <SearchableDropdown dropdownOptions={hours()} onChange={this.updateToHour} />
          <SearchableDropdown dropdownOptions={timeOfDay()} onChange={this.updateToPeriod} />
        </InstructableInput>
        </div>
      </div>
    );
  }
}

function hours() {
  return [
    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12',
  ];
}

function timeOfDay() {
  return [
    'am', 'pm'
  ];
}
