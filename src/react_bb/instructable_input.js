import React, { PropTypes } from 'react';

const titleColors = {
  green: 'instructions-green',
}

export const InstructableInput = ({ children, instructions, secondaryInstructions, titleColor }) => {
  let titleClass = titleColors[titleColor];
  if (!titleClass) titleClass = 'instructions';

  return (
    <div className='instructable-input-box'>
      <label className={titleClass}>
        { instructions }
      </label>
      {
        secondaryInstructions ?
        (<label className='instructions'>
          { secondaryInstructions }
        </label>) : ''
      }
      <div className='input-box'>
        { children }
      </div>
    </div>
  );
};

InstructableInput.propTypes = {
  children: PropTypes.node,
  instructions: PropTypes.string,
  secondaryInstructions: PropTypes.string,
  titleClass: PropTypes.string,
};

export const InstructableInputGreen = ({ children, instructions, secondaryInstructions }) => {
  return (
    <InstructableInput instructions={instructions} titleColor='green' secondaryInstructions={secondaryInstructions}>
      { children }
    </InstructableInput>
  );
};

InstructableInputGreen.propTypes = {
  children: PropTypes.node,
  instructions: PropTypes.string,
  secondaryInstructions: PropTypes.string,
  titleColor: PropTypes.string,
};
