import React from 'react';
import LinkBox from 'srcRoot/react_bb/link_box';
import GoalsPageLinkContainer from 'srcRoot/containers/goals_page_link_container';
import HappyPageLinkContainer from 'srcRoot/containers/happy_page_link_container';
import UnhappyPageLinkContainer from 'srcRoot/containers/unhappy_page_link_container';
import ActivityZonesPageLink from 'srcRoot/components/activity_zones_page_link';

const ObjectivePage = ({ updatePageVisibility }) => (
  <div className='objective-page'>
    <GoalsPageLinkContainer />
    <HappyPageLinkContainer />
    <UnhappyPageLinkContainer />
    <ActivityZonesPageLink />
  </div>
);

export default ObjectivePage;
