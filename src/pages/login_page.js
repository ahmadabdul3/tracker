import React, { Component } from 'react';
import FormGroup from 'srcRoot/react_bb/form_group';
import { InstructableInput } from 'srcRoot/react_bb/instructable_input';
import { withRouter, Link } from 'react-router';

class LoginPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
    };
  }

  onChange = (e) => {
    const target = e.target;
    const name = target.name;
    const value = target.value;
    const newState = {};
    newState[name] = value;
    this.setState(newState);
  }

  login = () => {
    const { username, password } = this.state;
    if (username && password) {
      const { login, router } = this.props;
      login({username: username.trim(), password });
      router.push('/');
    }
  }

  render() {
    return (
      <div className='login-page'>
        <div className='content-wrapper'>
          <div className='logo'>
            <span className='therapy'>
              Therapy
            </span>
            <span className='hub'>
              Hub
            </span>
          </div>
          <FormGroup title='Login'>
            <div>
              <InstructableInput instructions='Username - Required'>
                <input className='input' name='username' value={this.state.username} onChange={this.onChange} />
              </InstructableInput>
              <InstructableInput instructions='Password - Required'>
                <input className='input' type='password' name='password' value={this.state.password} onChange={this.onChange} />
              </InstructableInput>
            </div>
            <div className='centered-button-box'>
              <button className='primary-btn' onClick={this.login}>
                Login
              </button>
            </div>
            <div className='login-page-links'>
              <Link to='/signup' className='link'>
                Sign Up for TherapyHub
              </Link>
            </div>
          </FormGroup>
        </div>
      </div>
    );
  }
}

export default withRouter(LoginPage);
