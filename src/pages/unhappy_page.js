import React from 'react';
import ThingsMakeMeUnhappyContainer from 'srcRoot/containers/things_make_me_unhappy_container';
import LinkedPageContainer from 'srcRoot/containers/linked_page_container';

const UnhappyPage = ({ updatePageVisibility }) => (
  <LinkedPageContainer backLink='/objectives' title='Unhappiness'>
    <div className='objective-page'>
      <ThingsMakeMeUnhappyContainer />
    </div>
  </LinkedPageContainer>
);

export default UnhappyPage;
