import React from 'react';
import ThingsMakeMeHappyContainer from 'srcRoot/containers/things_make_me_happy_container';
import LinkedPageContainer from 'srcRoot/containers/linked_page_container';

const HappyPage = ({ updatePageVisibility }) => (
  <LinkedPageContainer backLink='/objectives' title='Happiness'>
    <div className='objective-page'>
      <ThingsMakeMeHappyContainer />
    </div>
  </LinkedPageContainer>
);

export default HappyPage;
