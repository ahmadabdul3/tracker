import React from 'react';
import { Link } from 'react-router';
import ReviewFeedbackInfoBox from 'srcRoot/react_bb/review_feedback_info_box';
import RatingStars from 'srcRoot/react_bb/rating_stars';

const TherapistPage = ({ reviews }) => {
  return (
    <div className='therapist-page'>

      <div className='therapist-header-info-box'>
        <div className='content hrz-img-txt-combo'>
          <div className='img-box image-container'>
            <img className='img'
              src='http://zt-four.zoodemo.com/images/article/mark3.jpg'/>
          </div>
          <div className='text'>
            <h3>
              Eric Shafer
            </h3>
            <RatingStars />
          </div>
        </div>
      </div>

      <div className='therapist-actions'>
        <div className='two-hrz-btns-box'>
          <Link className='btn' to='therapist/schedule'>
            <div className='hrz-img-txt-combo'>
              <div className='img-box'>
                <i className="fa fa-lg fa-calendar" aria-hidden="true"></i>
              </div>
              <h5>
                Book Me
              </h5>
            </div>
          </Link>
          <Link className='btn' to='therapist/review'>
            <div className='hrz-img-txt-combo'>
              <div className='img-box'>
                <i className="fa fa-lg fa-star" aria-hidden="true"></i>
              </div>
              <h5>
                Write a Review
              </h5>
            </div>
          </Link>
        </div>
      </div>

      <div className='therapist-about-info-box'>
        <h3 className='content title'>
          About
        </h3>
        <p className='content body'>
          Hi, my name is Eric and
          I focus on Lorem ipsum dolor sit amet, has populo consequuntur definitiones te.
          Aliquip principes sea id. Qui ne tamquam temporibus cotidieque, at per
          omnis aliquam, enim aliquando sea ex.
        </p>
      </div>


      <h3 className='section-title'>
        What people think of me
      </h3>

      <div className='reviews'>
        <ReviewFeedbackInfoBox />
        <ReviewFeedbackInfoBox />
          {
            reviews ? renderReviews(reviews) : ''
          }
      </div>

    </div>
  );
}

export default TherapistPage;

function renderReviews(reviews) {
  return reviews.map((review, key) => {
    return (
      <ReviewFeedbackInfoBox
        key={key}
        title={review.title}
        body={review.body}
        starRating={review.starRating} />
    );
  })
}
