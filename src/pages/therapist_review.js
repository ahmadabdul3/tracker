import React, { PropTypes, Component } from 'react';
import FormGroup from 'srcRoot/react_bb/form_group';
import ReviewableStars from 'srcRoot/components/reviewable_stars';
import { InstructableInput } from 'srcRoot/react_bb/instructable_input';
import { Link, withRouter } from 'react-router';
import LinkedPageContainer from 'srcRoot/containers/linked_page_container';

class TherapistReview extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      body: '',
      starRating: 0,
    }
  }

  onSubmit = () => {
    if (!this.formIsValid()) return;
    const { addReview, router } = this.props;
    addReview(this.state);
    router.push('/therapist');
  }

  cancel = () => {
    this.props.router.push('/therapist');
  }

  formIsValid() {
    const { title, body, starRating } = this.state;
    if (!title || !body || !starRating) return false;
    return true;
  }

  onChange = (e) => {
    const target = e.target;
    const value = target.value;
    const name = target.name;
    this.setState({ [name]: value });
  }

  getStarRating = (starRating) => {
    this.setState({ starRating });
  }

  render() {
    return (
      <LinkedPageContainer backLink='/therapist' title='Review'>
        <div className='therapist-review'>
          <FormGroup
            title='Review your therapist'
            subtitle='Please provide a star rating, a title, and details about your experience with your therapist'
            additionalInstructions='All fields are required'>
            <ReviewableStars starClick={this.getStarRating} />
            <div className='review-inputs'>
              <InstructableInput instructions='Title'>
                <input name='title' type='text' className='input' onChange={this.onChange} />
              </InstructableInput>
              <InstructableInput instructions='Experience with therapist'>
                <textarea name='body' className='review-body' onChange={this.onChange}/>
              </InstructableInput>
            </div>
            <div className='form-buttons'>
              <button className='btn' onClick={this.cancel}>
                Nevermind
              </button>
              <button className='submit-btn' onClick={this.onSubmit}>
                Submit
              </button>
            </div>
          </FormGroup>
        </div>
      </LinkedPageContainer>
    );
  }
}

export default withRouter(TherapistReview);
