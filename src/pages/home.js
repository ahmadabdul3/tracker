import React from 'react';
import { Link } from 'react-router';
import DailyStatusLinkContainer from 'srcRoot/containers/daily_status_link_container';
import LabeledData from 'srcRoot/react_bb/labeled_data';
import AppointmentsLinkContainer from 'srcRoot/containers/appointments_link_container';
import StageOfChangeLink from 'srcRoot/components/stage_of_change_link';

const HomePage = () => (
  <div className='home-page'>
    <div className='background'></div>
    <header>
      <div className='overlay'></div>
      <div className='background-img-box'>
        <img className='background'
        src='http://cdn.makeuseof.com/wp-content/uploads/2015/12/Zenscene_shutterstock_268875851.jpg?180b61'/>
      </div>
      <div className='content image-content-box'>
        <div>
          <div className='image-container'>
            <img className='img'
            src='http://www.fidleronthetooth.com/wp-content/uploads/2013/04/smiley.jpg'/>
          </div>
        </div>
        <label className='user-name'>
          Jane Spencer
        </label>
      </div>
    </header>

    <DailyStatusLinkContainer />
    <AppointmentsLinkContainer />
    <StageOfChangeLink />
  </div>
);

export default HomePage;

/*

<div className='header-info-box'>
  <div className='content'>
    <LabeledData label='Stage of Change' data='4' />
    <LabeledData label='Points' data='248' />
    <LabeledData label='badges' data='4' />
  </div>
</div>
*/

/*

  <div className='next-appointment'>
      <div className='component-icon gen-abs-content therapist-image'>
        <Link className='image-container-xsm' to='/therapist'>
          <div className='category hidden'>therapist</div>
          <img className='img'
            src='http://zt-four.zoodemo.com/images/article/mark3.jpg'/>
        </Link>
      </div>
      <div className='content'>
        <h4 className='title'>
          Your next meeting
        </h4>
        <div className='info-items'>
          <div className='info-item'>
            <label className='therapist-name'>
              Eric Shafer
            </label>
          </div>
          <div className='info-item'>
            <div className='seperator-dot'></div>
          </div>
          <div className="info-item">
            <span className='text vertical-center'>
              Mon, Aug 14, 2016
            </span>
          </div>
          <div className='info-item'>
            <div className='seperator-dot'></div>
          </div>
          <div className="info-item">
            <span className='text vertical-center'>
              2d, 5h, 32m
            </span>
          </div>
        </div>
      </div>
    </div>

*/
