import React, { Component } from 'react';
const io = require('socket.io-client');

export default class ChatPage extends Component {
  constructor(props) {
    super(props);
    this.socket = io();
    this.socket.on('message', (msg) => {
      const { messages } = this.state;
      messages.push(msg);
      this.setState({ messages });
    });
    this.state = {
      newMessage: '',
      messages: [],
    };
  }

  componentWillUnmount() {
    this.socket.emit('manualDisconnect');
  }

  sendMessage = () => {
    const message = this.state.newMessage;
    const { username } = this.props.login;
    if (message) {
      this.socket.emit('message', { message, username });
      this.setState({
        newMessage: '',
      });
    }
  }

  updateNewMessage = (e) => {
    const newMessage = e.target.value;
    this.setState({ newMessage });
  }

  getMessageClass(message) {
    const username = message.username;
    if (username === this.props.login.username) {
      return 'text-message self';
    }

    return 'text-message other';
  }

  render() {
    const { messages } = this.state;
    const buttonClass = this.state.newMessage ? 'primary-btn' : 'btn';

    return (
      <div className='chat-page'>
        <div className='messages'>
          {
            messages.map((message, key) => {
              const messageClass = this.getMessageClass(message);
              return (<div className={messageClass} key={key}>
                <div className='inner'>
                  <div className='username'>
                    { message.username }
                  </div>
                  <div className='message'>
                    { message.message }
                  </div>
                </div>
              </div>)
            })
          }
        </div>
        <div className='message-input'>
          <input value={this.state.newMessage} onChange={this.updateNewMessage} className='input' />
          <button className={buttonClass} onClick={this.sendMessage}>
            Send
          </button>
        </div>
      </div>
    );
  }
}


/**

  - tokbox stuff:

  var apiKey = '45806122';
  var sessionId = '2_MX40NTgwNjEyMn5-MTQ5MDU3ODE0NjE0MX5WeENWUmNKb2ViMzFpWTJlL3BlWENSQ1h-fg';
  var token = `T1==cGFydG5lcl9pZD00NTgwNjEyMiZzaWc9MWQxMzk3MzEwODY1MzRkYmUzMzdlNTI3MTg1OWM1YmVjYjY5ODIwMjpzZXNzaW9uX2lkPTJfTVg0ME5UZ3dOakV5TW41LU1UUTVNRFUzT0RFME5qRTBNWDVXZUVOV1VtTktiMlZpTXpGcFdUSmxMM0JsV0VOU1ExaC1mZyZjcmVhdGVfdGltZT0xNDkwNTc4MjEwJm5vbmNlPTAuNzYxNzk4NTU5NzUzMDQxNCZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNDkwNTgxODA5`;
  var session = OT.initSession(apiKey, sessionId)
    .on('streamCreated', function(event) {
      session.subscribe(event.stream);
    })
    .connect(token, function(error) {
      var publisher = OT.initPublisher();
      session.publish(publisher);
    });

*/
