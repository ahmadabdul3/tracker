import React, { Component, PropTypes } from 'react';
import LinkedPageContainer from 'srcRoot/containers/linked_page_container';
import AppointmentSummary from 'srcRoot/react_bb/appointment_summary';
import { withRouter } from 'react-router';


class Appointments extends Component {
  static propTypes = {
    appointments: PropTypes.array,
    deleteAppointment: PropTypes.func,
  }

  constructor(props) {
    super(props);
  }

  goToTherapistSchedule = () => {
    const { setBackLink, router } = this.props;
    setBackLink('/appointments');
    router.push('therapist/schedule');
  }

  render() {
    const { appointments, deleteAppointment } = this.props;

    return (
      <LinkedPageContainer backLink='/' title='Appointments'>
        <div className='form-box'>
          Welcome to the appointments page. Here you can view or delete your scheduled appointments
        </div>
        {
          appointments && appointments.length > 0 ?
          (
            <div>
              {appointments.map((appointment, key) => {
                return <AppointmentSummary data={appointment} key={key} deleteAppointment={deleteAppointment} deletable />
              })}
              <div className='centered-button-box'>
                <button className='primary-btn' onClick={this.goToTherapistSchedule}>
                  Add another appointment
                </button>
              </div>
            </div>
          )
          :
          ( <div className='form-box' onClick={this.goToTherapistSchedule}>
              <div className='no-data-text'>
                Looks like You don't have any appointments scheduled right now. <span className='link'>
                Click here </span> to go to your therapist's schedule and reserve one
              </div>
            </div> )
        }
      </LinkedPageContainer>
    );
  }
}

export default withRouter(Appointments);
