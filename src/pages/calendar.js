import React from 'react';
import CalendarEntry from 'srcRoot/react_bb/calendar_entry';

const CalendarPage = ({ updatePageVisibility }) => (
  <div className='calendar-page'>
    <div className='calendar-group'>
      <div className='title-bar'>
        December
      </div>

      <CalendarEntry updatePageVisibility={updatePageVisibility} />
      <CalendarEntry updatePageVisibility={updatePageVisibility} />
      <CalendarEntry updatePageVisibility={updatePageVisibility} />
      <CalendarEntry updatePageVisibility={updatePageVisibility} />

    </div>

    <div className='calendar-group'>
      <div className='title-bar'>
        January
      </div>

      <CalendarEntry updatePageVisibility={updatePageVisibility} />
      <CalendarEntry updatePageVisibility={updatePageVisibility} />
      <CalendarEntry updatePageVisibility={updatePageVisibility} />
      <CalendarEntry updatePageVisibility={updatePageVisibility} />
      <CalendarEntry updatePageVisibility={updatePageVisibility} />
      <CalendarEntry updatePageVisibility={updatePageVisibility} />

    </div>

    <div className='calendar-group'>
      <div className='title-bar'>
        February
      </div>

      <CalendarEntry updatePageVisibility={updatePageVisibility} />
      <CalendarEntry updatePageVisibility={updatePageVisibility} />
      <CalendarEntry updatePageVisibility={updatePageVisibility} />
      <CalendarEntry updatePageVisibility={updatePageVisibility} />

    </div>
  </div>
);

export default CalendarPage;
