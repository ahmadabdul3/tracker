import React, { Component, PropTypes } from 'react';

export default class NotificationsPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: [
        'A new activity has been scheduled for you',
        'Your video call with Eric Shafer will be starting in 10 minutes',
        'Youve earned 10 points for completing a goal',
      ],
    };
  }

  render() {
    const { messages } = this.state;
    const { notifications } = this.props;

    return (
      <div className='notifications'>
        {messages.map((message, key) => {
          return <Notification message={message} key={key} />
        })}
        {notifications.map((notification, key) => {
          return <Notification message={formatScheduledActivity(notification)} key={key} />
        })}
      </div>
    );
  }
}

function formatScheduledActivity(scheduledActivity) {
  const { event, activity, day, fromTime, toTime } = scheduledActivity;
  return event + ': ' + activity + ', ' + day + ' ' + fromTime.hour + ' ' + fromTime.periodOfDay;
}


function Notification({ message }) {
  return (
    <div className='notification'>
      <div className='icon'>
        <i className="fa fa-lg fa-bell" aria-hidden="true" />
      </div>
      <h3 className='title'>
        { message }
      </h3>
    </div>
  );
}
