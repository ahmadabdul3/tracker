import React, { Component } from 'react';
import TimelineEntry from 'srcRoot/react_bb/timeline_entry';


export default class TherapyTimelinePage extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { timeline, appointments } = this.props;

    return (
      <div className='timeline-page'>
        {
          timeline && timeline.length > 0 ?
          timeline.map((group, key) => {
            return (<TimelineGroup group={group} appointments={appointments} key={key} />)
          }) :
          (<div className='form-box'>
            <div className='no-data-text'>
              You don't have any timeline entries yet
            </div>
          </div>)
        }
      </div>
    );
  }
}


function TimelineGroup({ group, appointments }) {
  return (
    <div className='timeline-entry-group'>
      {
        group.dayEntries.map((entry, key) => {
          return <TimelineEntry entry={entry} appointments={appointments} key={key} />
        })
      }
      <div className='timeline-entry title'>
        <div className='date'>
          <div className='timeline-location-dot' ></div>
        </div>
        <div className='info'>
          <h3 className='content'>
            { group.month }
          </h3>
        </div>
      </div>
    </div>
  )
}
