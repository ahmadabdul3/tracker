import React, { Component } from 'react';
import FormGroup from 'srcRoot/react_bb/form_group';
import { InstructableInput } from 'srcRoot/react_bb/instructable_input';
import { withRouter, Link } from 'react-router';

class SignUpPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      username: '',
      password: '',
      questionNumber: 0,
      questions: [
        1, 2, 3, 4, 5
      ],
    }
  }

  onChange = (e) => {
    const { target } = e;
    const { value, name } = target;

    this.setState({ [name]: value });
  }

  proceedToNextStep = () => {
    const { router, alertInfo } = this.props;
    const { questionNumber, questions } = this.state;
    if (questionNumber < questions.length) {
      this.setState({ questionNumber: this.state.questionNumber + 1 });
    } else if (questionNumber === questions.length) {
      alertInfo({ type: '', text: "You've been automatically matched with Eric Shafer as your therapist!" });
      router.push('/');
    }
  }

  getQuestion() {
    const { questionNumber } = this.state;
    return <FirstQuestion questionNumber={questionNumber} />;
  }

  render() {
    const { questionNumber, questions } = this.state;

    return (
      <div className='signup-page'>
        <div className='logo'>
          <span className='therapy'>
            Therapy
          </span>
          <span className='hub'>
            Hub
          </span>
        </div>
        <div className='registration-form'>
          {
            questionNumber === 0 ?
            (<FormGroup title='Sign Up'>
              <div>
                <InstructableInput instructions='First Name - Required'>
                  <input className='input' name='firstName' value={this.state.firstName} onChange={this.onChange} />
                </InstructableInput>
                <InstructableInput instructions='Last Name - Required'>
                  <input className='input' name='lastName' value={this.state.lastName} onChange={this.onChange} />
                </InstructableInput>
                <InstructableInput instructions='Username - Required'>
                  <input className='input' name='username' value={this.state.username} onChange={this.onChange} />
                </InstructableInput>
                <InstructableInput instructions='Password - Required'>
                  <input className='input' type='password' name='password' value={this.state.password} onChange={this.onChange} />
                </InstructableInput>
              </div>
              <div className='centered-button-box'>
                <button className='primary-btn' onClick={this.proceedToNextStep}>
                  Create Account
                </button>
              </div>
              <div className='signup-page-links'>
                <Link to='/login' className='link'>
                  Already have an account?
                </Link>
              </div>
            </FormGroup>)
            :
            (<div>
              <div className='form-box sign-up-questions-disclaimer'>
                <p className='main-explanation'>
                  Please answer the following questions so that the app can
                </p>
                <ul className='list-of-benefits'>
                  <li className='benefit'>
                    - Be tailored to your specific needs
                  </li>
                  <li className='benefit'>
                    - Match you to the best therapist based on your needs
                  </li>
                </ul>
              </div>
              <div className='number-of-questions'>
                Question { questionNumber } of { questions.length }
              </div>
              { this.getQuestion() }
              <div className='centered-button-box'>
                <button className='primary-btn' onClick={this.proceedToNextStep}>
                  Next Question
                </button>
              </div>
            </div>)
          }
        </div>
      </div>
    );
  }
}

export default withRouter(SignUpPage);

function FirstQuestion({ questionNumber }) {
  return (
    <FormGroup title={`Question number ${questionNumber}`}>
      Some questions
    </FormGroup>
  );
}
