import React from 'react';
import GoalsFormContainer from 'srcRoot/containers/goals_form_container';
import LinkedPageContainer from 'srcRoot/containers/linked_page_container';

const GoalsPage = ({ updatePageVisibility }) => (
  <LinkedPageContainer backLink='/objectives' title='Goals'>
    <div className='objective-page'>
      <GoalsFormContainer />
    </div>
  </LinkedPageContainer>
);

export default GoalsPage;
