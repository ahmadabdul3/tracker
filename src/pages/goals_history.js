import React, { Component } from 'react';
import LinkedPageContainer from 'srcRoot/containers/linked_page_container';

export default class GoalsHistoryPage extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { goals } = this.props;
    const statusTitlecaseMap = {
      abandoned: 'Abandoned',
      accomplished: 'Accomplished',
    };
    const statusIconMap = {
      abandoned: 'fa-times',
      accomplished: 'fa-flag-checkered',
    }

    return (
      <LinkedPageContainer backLink='/objectives/goals' title='Goal History'>
        <div className='form-box'>
          Welcome to the goal history page. Here you can view goals that you've either
          completed or abandoned
        </div>
        {
          goals && goals.length > 0 ?
          (
            goals.map((goal, key) => {
              if (goal.status !== 'inProgress') {
                return (
                  <div className='goal-history-item' key={key}>
                    <div className='value'>
                      { goal.value }
                    </div>
                    <div className={`status ${goal.status}`}>
                      { statusTitlecaseMap[goal.status] }
                      <i className={`fa ${statusIconMap[goal.status]}`} aria-hidden="true" />
                    </div>
                  </div>
                )
              }
            })
          ) : null
        }
      </LinkedPageContainer>
    );
  }
}
