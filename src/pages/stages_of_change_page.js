import React from 'react';
import LinkedPageContainer from 'srcRoot/containers/linked_page_container';

export default function StagesOfChangePage({ }) {
  return (
    <LinkedPageContainer backLink='/' title='Stages of Change'>
    <div className='form-box'>
      Below are the stages of change.
    </div>
      <div className='form-box'>
        <img
          className=''
          src='http://2.bp.blogspot.com/-PhlKUs6CTtc/VV8r3SpeLHI/AAAAAAAAIRk/vH8LUG0pJ0k/s1600/Cycle_of_Change_Image_-300x285.png'/>
      </div>
    </LinkedPageContainer>
  );
}
