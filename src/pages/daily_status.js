import React from 'react';
import DailyStatusContainer from 'srcRoot/containers/daily_status_container';
import LinkedPageContainer from 'srcRoot/containers/linked_page_container';

const DailyStatus = () => (
  <LinkedPageContainer backLink='/' title='Daily Status'>
    <div className=''>
      <DailyStatusContainer />
    </div>
  </LinkedPageContainer>
);

export default DailyStatus;
