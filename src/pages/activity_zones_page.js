import React from 'react';
import LinkedPageContainer from 'srcRoot/containers/linked_page_container';
import ActivityZonesFormContainer from 'srcRoot/containers/activity_zones_form_container';

const ActivityZonesPage = ({ updatePageVisibility }) => (
  <LinkedPageContainer backLink='/objectives' title='Activity Zones'>
    <div className='objective-page'>
      <ActivityZonesFormContainer />
    </div>
  </LinkedPageContainer>
);

export default ActivityZonesPage;
