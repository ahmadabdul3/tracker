import React, { Component, PropTypes } from 'react';
import { Link, withRouter } from 'react-router';
import LinkedPageContainer from 'srcRoot/containers/linked_page_container';
import CalendarEntry from 'srcRoot/react_bb/calendar_entry';
import AvailabilityEntry from 'srcRoot/react_bb/availability_entry';

export default class TherapistSchedulePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      therapistSchedule: props.therapistSchedule
    }
  }

  componentDidReceiveProps(props) {
    this.setState({ therapistSchedule: props.therapistSchedule });
  }

  render() {
    const { addAppointment, deleteTimeframe, addEvent } = this.props;
    const { therapistSchedule } = this.state;

    return (
      <LinkedPageContainer backLink='/therapist' title='Schedule'>
        {
          therapistSchedule && therapistSchedule.length > 0 ? (
            <div className='calendar-group'>
              {therapistSchedule.map((entry, key) => {
                return (
                  <MonthEntry
                    entry={entry}
                    addAppointment={addAppointment}
                    deleteTimeframe={deleteTimeframe}
                    addEvent={addEvent}
                    key={key} />
                )
              })}
            </div>
          ) : null
        }
      </LinkedPageContainer>
    );
  }
}

function MonthEntry({ entry, addAppointment, deleteTimeframe, addEvent }) {
  return (
    <div>
      <div className='title-bar'>
        { entry.month }
      </div>

      <div className='entries'>
        {
          entry.availableDays.map((availableDay, key) => {
            return (
              <AvailabilityEntry
                monthId={entry.id}
                data={availableDay}
                addAppointment={addAppointment}
                deleteTimeframe={deleteTimeframe}
                addEvent={addEvent}
                key={key} />
            )
          })
        }
      </div>
    </div>
  );
}
