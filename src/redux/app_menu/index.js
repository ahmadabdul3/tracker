const actions = {};

actions.hideAppMenu = function() {
  return {
    type: 'HIDE_APP_MENU',
  };
};

actions.openAppMenu = function() {
  return {
      type: 'OPEN_APP_MENU',
  }
};

export { actions };

const initialState = {
  hidden: true,
};

export default function appMenu(state = initialState, action) {
  switch (action.type) {
    case 'HIDE_APP_MENU':
      return {
        hidden: true,
      };

    case 'OPEN_APP_MENU':
      return {
        hidden: false,
      };

    default: return state;
  }
}
