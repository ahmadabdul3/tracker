const actions = {};

actions.setBackLink = function(data) {
  return {
    type: 'SET_BACK_LINK',
    data,
  }
};

actions.clearBackLink = function() {
  return {
    type: 'CLEAR_BACK_LINK',
  }
}

export { actions };

const initialState = {
  backLink: undefined,
};

export default function linkedPage(state = initialState, action) {
  switch (action.type) {
    case 'SET_BACK_LINK': return { backLink: action.data };
    case 'CLEAR_BACK_LINK': return initialState;

    default: return state;

  }
}
