const actions = {};

actions.login = function(data) {
  return {
    type: 'LOGIN',
    data,
  }
};

export { actions };

const initialState = {
  username: undefined,
};

export default function login(state = initialState, action) {
  switch (action.type) {
    case 'LOGIN': return { username: action.data.username };

    default: return state;

  }
}
