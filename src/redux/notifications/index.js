const actions = {};

actions.addNotification = function(data) {
  return {
    type: 'ADD_NOTIFICATION',
    data,
  };
}

export { actions };

const initialState = [];

export default function notifications(state = initialState, action) {
  switch (action.type) {
    case 'ADD_NOTIFICATION':
      return [...state, action.data];

    default:
      return state;
  }
}
