const actions = {};

actions.alertInfo = function(data) {
  return {
    type: 'ALERT_INFO',
    data,
  };
}

actions.removeAlert = function(id) {
  return {
    type: 'REMOVE_ALERT',
    id
  };
}

export { actions };

const initialState = [];

export default function alerts(state = initialState, action) {
  switch (action.type) {
    case 'ALERT_INFO':
      const alert = {
        id: state.length > 0 ? state[0].id + 1 : 1,
        data: action.data,
      }
      return [
        ...state,
        action.data
      ];

    case 'REMOVE_ALERT':
      const theState = [...state];
      const indexOfAlert = state.findIndex((alert) => {
        return alert.id === action.id;
      });
      theState.splice(action.id, 1);
      return theState;

    default: return state;
  }
}
