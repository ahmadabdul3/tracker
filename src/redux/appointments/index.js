const actions = {};

actions.addAppointment = function(data) {
  return {
    type: 'ADD_APPOINTMENT',
    data,
  };
};

actions.deleteAppointment = function(data) {
  return {
    type: 'DELETE_APPOINTMENT',
    data,
  }
};

export { actions };

const initialState = [];

export default function appointments(state = initialState, action) {
  switch (action.type) {
    case 'ADD_APPOINTMENT':
      return [...state, action.data];

    case 'DELETE_APPOINTMENT':
      const itemToDeleteIndex = state.findIndex((appointment) => {
        return (appointment.id === action.data.id && appointment.timeframe.id === action.data.timeframe.id);
      });
      const appointments = [...state];
      appointments.splice(itemToDeleteIndex, 1);
      return appointments;

    default: return state;
  }
}
