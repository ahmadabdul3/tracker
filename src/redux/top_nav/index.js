const actions = {};

actions.showSeparator = function(data) {
  return {
    type: 'SHOW_SEPARATOR',
  };
}

actions.hideBackgroundColor = function() {
  return {
    type: 'HIDE_BACKGROUND_COLOR',
  };
}

actions.showBackgroundColor = function() {
  return {
    type: 'SHOW_BACKGROUND_COLOR',
  };
}

actions.hideSeparator = function(data) {
  return {
    type: 'HIDE_SEPARATOR',
  };
}

export { actions };

const initialState = {
  renderSeparator: true,
  backgroundColorVisible: true,
};

function topNav(state = initialState, action) {
  switch (action.type) {
    case 'SHOW_SEPARATOR':
      return {
        ...state,
        renderSeparator: true,
      };

    case 'HIDE_SEPARATOR':
      return {
        ...state,
        renderSeparator: false,
      }

    case 'HIDE_BACKGROUND_COLOR':
      return {
        ...state,
        backgroundColorVisible: false,
      }

    case 'SHOW_BACKGROUND_COLOR':
      return {
        ...state,
        backgroundColorVisible: true,
      }

    default:
      return state;
  }
}

export default topNav;
