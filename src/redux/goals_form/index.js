const actions = {};

actions.updateGoals = function(data) {
  return {
    type: 'UPDATE_GOALS',
    data,
  };
}

export { actions };

const initialState = [];

export default function goals(state = initialState, action) {
  switch (action.type) {
    case 'UPDATE_GOALS': return action.data;

    default: return state;

  }
}
