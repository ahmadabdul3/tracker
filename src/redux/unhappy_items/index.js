const actions = {};

actions.updateUnhappyItems = function(data) {
  return {
    type: 'UPDATE_UNHAPPY_ITEMS',
    data,
  }
};

export { actions };

const initialState = [];

export default function unhappyItems(state = initialState, action) {
  switch (action.type) {
    case 'UPDATE_UNHAPPY_ITEMS': return action.data;

    default: return state;

  }
}
