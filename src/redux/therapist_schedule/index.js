const actions = {};

actions.deleteTimeframe = function(data) {
  return {
    type: 'DELETE_TIMEFRAME',
    data,
  }
};

export { actions };

const initialState = getInitialState();

export default function therapistSchedule(state = initialState, action) {
  switch (action.type) {
    case 'DELETE_TIMEFRAME':
      const returnState = [...state];
      const { data } = action;
      const monthIndex = state.findIndex((month) => {
        return (month.id === data.monthId);
      });
      const month = state[monthIndex];
      const availableDayIndex = month.availableDays.findIndex((day) => {
        return day.id === data.dayId;
      });
      const availableDays = month.availableDays[availableDayIndex];
      const timeframeIndex = state[monthIndex].availableDays[availableDayIndex].availableTimeframes.findIndex((timeframe) => {
        return timeframe.id === data.timeframeId;
      });
      returnState[monthIndex].availableDays[availableDayIndex].availableTimeframes.splice(timeframeIndex, 1);
      return returnState;

    default: return state;
  }
}

function getInitialState() {
  return [
    {
      id: 1,
      month: 'April',
      availableDays: [
        {
          id: 1,
          day: 'Monday',
          number: 10,
          month: 'December',
          year: 2017,
          availableTimeframes: [
            {
              id: 1,
              startTime: {
                hour: '5:00',
                periodOfDay: 'pm',
              },
              endTime: {
                hour: '5:25',
                periodOfDay: 'pm',
              }
            },
            {
              id: 2,
              startTime: {
                hour: '5:30',
                periodOfDay: 'pm',
              },
              endTime: {
                hour: '5:55',
                periodOfDay: 'pm',
              }
            }
          ]
        },
        {
          id: 2,
          day: 'Wednesday',
          number: 12,
          month: 'December',
          year: 2017,
          availableTimeframes: [
            {
              id: 3,
              startTime: {
                hour: '6:30',
                periodOfDay: 'pm',
              },
              endTime: {
                hour: '6:55',
                periodOfDay: 'pm',
              }
            },
            {
              id: 4,
              startTime: {
                hour: '7:00',
                periodOfDay: 'pm',
              },
              endTime: {
                hour: '7:25',
                periodOfDay: 'pm',
              }
            },
          ]
        },
        {
          id: 3,
          day: 'Friday',
          number: 14,
          month: 'December',
          year: 2017,
          availableTimeframes: [
            {
              id: 5,
              startTime: {
                hour: '6:00',
                periodOfDay: 'pm',
              },
              endTime: {
                hour: '6:25',
                periodOfDay: 'pm',
              }
            },
          ]
        },
      ]
    }
  ];
}
