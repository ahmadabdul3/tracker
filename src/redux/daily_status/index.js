const actions = {};

actions.submitDailyStatus = function(data) {
  return {
    type: 'SUBMIT_DAILY_STATUS',
    data,
  };
}

export { actions };

const initialState = {
  feeling: undefined,
  thoughts: undefined,
  submitted: false,
};

export default function dailyStatus(state = initialState, action) {
  switch (action.type) {
    case 'SUBMIT_DAILY_STATUS':
      return {
        ...action.data,
        submitted: true,
      };

    default: return state;
  }
}
