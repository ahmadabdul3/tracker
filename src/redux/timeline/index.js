const actions = {};

actions.addEvent = (data) => {
  return {
    type: 'ADD_EVENT',
    data
  }
}

export { actions };

const initialState = getInitialState();

export default function timeline(state = initialState, action) {
  switch (action.type) {
    case 'ADD_EVENT':
      const eventKey = action.data.eventKey;
      const month = state[0];
      const today = month.dayEntries[0];
      today.events[eventKey] = action.data.event;
      return [...state];

    default:
      return state;
  }
}


function getInitialState() {
  return [
    {
      month: 'April',
      dayEntries: [
        {
          day: 'Thu',
          number: 17,
          events: {
            stageOfChange: {
              label: 'Stage of Change',
              value: 4,
            },
          }
        },
        {
          day: 'Wed',
          number: 16,
          events: {
            stageOfChange: {
              label: 'Stage of Change',
              value: 4,
            },
            feeling: {
              label: 'Feeling',
              value: 'Positive',
            },
            thoughts: {
              label: 'Thoughts',
              value: 'Worked out with the help of the apps auto-scheduler',
            },
            goals: {
              label: 'Added goals',
              value: [
                {
                  value: 'Exercise twice a week',
                  status: 'inProgress',
                },
                {
                  value: 'eat healthy',
                  status: 'inProgress',
                }
              ],
            },
            happyItems: {
              label: 'Added happy items',
              value: ['Exercise', 'Eating healthy'],
            }
          },
        },
        {
          day: 'Tue',
          number: 15,
          events: {
            stageOfChange: {
              label: 'Stage of Change',
              value: 4
            },
            feeling: {
              label: 'Feeling',
              value: 'Neutral',
            },
            thoughts: {
              label: 'Thoughts',
              value: 'Didnt do anything today, bored.',
            },
            appointment: {
              label: 'Appointments',
              value: 'Eric Shafer'
            }
          },
        },
        {
          day: 'Mon',
          number: 14,
          events: {
            stageOfChange: {
              label: 'Stage of Change',
              value: 4
            },
            feeling: {
              label: 'Feeling',
              value: 'Positive',
            },
            thoughts: {
              label: 'Thoughts',
              value: 'Enjoyed a good lunch with my best friend',
            },
          },
        },
      ],
    },
  ];
}
