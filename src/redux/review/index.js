const actions = {};

actions.addReview = function(data) {
  return {
    type: 'ADD_REVIEW',
    data,
  };
}

export { actions };

const initialState = {
  reviews: [],
};

export default function review(state = initialState, action) {
  switch (action.type) {
    case 'ADD_REVIEW':
      return {
        reviews: [...state.reviews, action.data],
      };

    default:
      return state;
  }
}
