const actions = {};

actions.updateHappyItems = function(data) {
  return {
    type: 'UPDATE_HAPPY_ITEMS',
    data,
  }
};

export { actions };

const initialState = [];

export default function happyItems(state = initialState, action) {
  switch (action.type) {
    case 'UPDATE_HAPPY_ITEMS': return action.data;

    default: return state;

  }
}
