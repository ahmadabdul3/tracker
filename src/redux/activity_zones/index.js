const actions = {};

actions.addActivityZone = function(data) {
  return {
    type: 'ADD_ACTIVITY_ZONE',
    data,
  };
}

export { actions };

const initialState = [
  // {
  //   activity: 'some activity',
  //   day: 'Monday',
  //   fromTime: {
  //     hour: '5',
  //     periodOfDay: 'pm'
  //   },
  //   toTime: {
  //     hour: '10',
  //     periodOfDay: 'pm'
  //   },
  // },
];

export default function activityZones(state = initialState, action) {
  switch (action.type) {
    case 'ADD_ACTIVITY_ZONE':
      return [
        ...state,
        action.data
      ];

    default: return state;
  }
}
