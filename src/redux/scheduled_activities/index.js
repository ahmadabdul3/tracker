const actions = {};

actions.scheduleActivity = function(data) {
  return {
    type: 'SCHEDULE_ACTIVITY',
    data,
  };
}

export { actions };

const initialState = [];

export default function scheduledActivities(state = initialState, action) {
  switch (action.type) {
    case 'SCHEDULE_ACTIVITY':
      return [...state, action.data];

    default:
      return state;
  }
}
