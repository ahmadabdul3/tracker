import { combineReducers } from 'redux';
import review from './review';
import dailyStatus from './daily_status';
import goals from './goals_form';
import happyItems from './happy_items';
import unhappyItems from './unhappy_items';
import topNav from './top_nav';
import activityZones from './activity_zones';
import appMenu from './app_menu';
import appointments from './appointments';
import therapistSchedule from './therapist_schedule';
import linkedPage from './linked_page';
import login from './login';
import timeline from './timeline';
import scheduledActivities from './scheduled_activities';
import notifications from './notifications';
import alerts from './alerts';

export default combineReducers({
  review,
  dailyStatus,
  goals,
  happyItems,
  unhappyItems,
  topNav,
  activityZones,
  appMenu,
  appointments,
  therapistSchedule,
  linkedPage,
  login,
  timeline,
  scheduledActivities,
  notifications,
  alerts,
});
