import React, { Component, PropTypes } from 'react';
import NavItem from 'srcRoot/react_bb/nav_item';
import { Link } from 'react-router';

export default class TopNav extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { renderSeparator, backgroundColorVisible } = this.props.topNav;
    const seperatorClass = renderSeparator ? '' : 'no-separator';
    const backgroundVisibilityClass = backgroundColorVisible ? '' : 'transparent';

    return (
      <div>
        <nav className={`std flex top unbordered-items thirds ${seperatorClass} ${backgroundVisibilityClass}`}>
          <div className='nav-item-box'>
            <div className='nav-item' onClick={this.props.openAppMenu}>
              <div className='content'>
                  <div className='icon-box'>
                    <i className="fa fa-lg fa-bars" aria-hidden="true" />
                  </div><div className='v-m-hack'></div>
              </div>
            </div>

            <NavItem>
              <i className="fa fa-lg fa-ils" aria-hidden="true"></i>
            </NavItem>

            <NavItem>
              <i className="fa fa-lg fa-search" aria-hidden="true"></i>
            </NavItem>
          </div>
        </nav>
      </div>
    );
  }
}
