import React, { Component } from 'react';

export default class Alerts extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { alerts, removeAlert } = this.props;
    return (
      <div className='alerts'>
        {
          alerts.map((alert, key) => {
            return <Alert data={alert} key={key} removeAlert={removeAlert} />
          })
        }
      </div>
    );
  }
}

class Alert extends Component {
  constructor(props) {
    super(props);

    setTimeout(() => {
      this.props.removeAlert(props.data.id);
    }, 8000);
  }

  getAlertMarkup() {
    const { data } = this.props;
    switch (data.type) {
      case 'scheduledActivity':
        return <ScheduledActivityAlert data={data}/>;

      default:
        return <TextAlert data={data} />;
    }
  }

  render() {
    const { data } = this.props;

    return (
      <div className='form-box alert'>
        { this.getAlertMarkup() }
      </div>
    );
  }
}

function TextAlert({ data }) {
  return (
    <div className=''>
      <i className='fa fa-info-circle info-icon' aria-hidden='true' />
      { data.text }
    </div>
  );
}

function ScheduledActivityAlert({ data }) {
  const { event, activity, day, fromTime, toTime } = data;
  const message = ', ' + day + ' ' + fromTime.hour + ' ' + fromTime.periodOfDay;

  return (
    <div className='scheduled-activity-alert'>
      <i className='fa fa-calendar info-icon' aria-hidden='true' />
      <span className='event'>{ event }</span>
      <div className='activity-details'>
        <span className='activity'>{activity}</span>{ message }
      </div>
    </div>
  );
}
