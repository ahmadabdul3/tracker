import React, { Component } from 'react';
import { withRouter } from 'react-router';
import NavItem from 'srcRoot/react_bb/nav_item';

export default class BottomNav extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <nav className='std flex bot'>
        <div className='nav-item-box'>
          <NavItem page=''>
            <i className="fa fa-lg fa-home lg" aria-hidden="true"></i>
          </NavItem>

          <NavItem page='chat'>
            <i className="fa fa-lg fa-comments-o" aria-hidden="true"></i>
          </NavItem>

          <NavItem page='objectives'>
            <i className="fa fa-lg fa-list" aria-hidden="true"></i>
          </NavItem>

          <NavItem page='timeline'>
            <i className="fa fa-lg fa-history lg" aria-hidden="true"></i>
          </NavItem>

          <NavItem page='notifications'>
            <i className="fa fa-lg fa-bell" aria-hidden="true"></i>
          </NavItem>
        </div>
      </nav>
    );
  }
}
