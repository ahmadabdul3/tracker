import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

export default class AppMenu extends Component {
  static propTypes = {
    hidden: PropTypes.bool,
  }

  constructor(props) {
    super(props);

    this.state = {
      hidden: true,
    }
  }

  componentWillReceiveProps(props) {
    this.setState({ hidden: props.appMenu.hidden });
  }

  hideMenu = () => {
    this.setState({ hidden: true });
  }

  render() {
    const { hidden } = this.state;
    let appMenuClass = 'app-menu';
    if (hidden) appMenuClass += ' hidden';

    return (
      <div className={appMenuClass}>
        <div className='title logo' onClick={this.hideMenu}>
          <span className='therapy'>Therapy</span><span className='hub'>Hub</span>
        </div>
        <Link to='/therapist' className='item' onClick={this.props.hideAppMenu}>
          My Therapist
        </Link>
        <Link to='/login' className='item' onClick={this.props.hideAppMenu}>
          Log In
        </Link>
        <Link to='/signup' className='item' onClick={this.props.hideAppMenu}>
          Sign Up
        </Link>
      </div>
    );
  }
}
